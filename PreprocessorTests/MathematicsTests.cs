﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PreprocessorUtils;
using ModelComponents;

namespace PreprocessorTests
{
    [TestClass]
    public class MathematicsTests
    {
        [TestMethod]
        public void Test_PointOnLine_positive_direction()
        {
            var line1 = new MyStraightLine(0, new MyPoint(0,0), new MyPoint(10, 10));
            Assert.IsTrue(Mathematics.pointOnLine(1, 1, line1));
            Assert.IsTrue(Mathematics.pointOnLine(7.01, 6.99, line1));
            Assert.IsTrue(Mathematics.pointOnLine(10, 10, line1));

            Assert.IsFalse(Mathematics.pointOnLine(2, 1, line1));
            Assert.IsFalse(Mathematics.pointOnLine(7, 7.5, line1));
            Assert.IsFalse(Mathematics.pointOnLine(10.5, 10.5, line1));
            Assert.IsFalse(Mathematics.pointOnLine(-5, -5, line1));
        }

        [TestMethod]
        public void Test_PointOnLine_negative_direction()
        {
            var line1 = new MyStraightLine(0, new MyPoint(10, 10), new MyPoint(0, 0));
            Assert.IsTrue(Mathematics.pointOnLine(1, 1, line1));
            Assert.IsTrue(Mathematics.pointOnLine(7.01, 6.99, line1));
            Assert.IsTrue(Mathematics.pointOnLine(10, 10, line1));

            Assert.IsFalse(Mathematics.pointOnLine(2, 1, line1));
            Assert.IsFalse(Mathematics.pointOnLine(7, 7.5, line1));
            Assert.IsFalse(Mathematics.pointOnLine(10.5, 10.5, line1));
            Assert.IsFalse(Mathematics.pointOnLine(-5, -5, line1));
        }

        [TestMethod]
        public void Test_PointOnLine_horizontal_line()
        {
            var line1 = new MyStraightLine(0, new MyPoint(10, 10), new MyPoint(20, 10));
            Assert.IsTrue(Mathematics.pointOnLine(11, 10, line1));
            Assert.IsTrue(Mathematics.pointOnLine(13.01, 9.99, line1));
            Assert.IsTrue(Mathematics.pointOnLine(10, 10, line1));

            Assert.IsFalse(Mathematics.pointOnLine(11, 11, line1));
            Assert.IsFalse(Mathematics.pointOnLine(12, 9, line1));
            Assert.IsFalse(Mathematics.pointOnLine(9.9, 10, line1));
            Assert.IsFalse(Mathematics.pointOnLine(20.1, 10, line1));
        }

        [TestMethod]
        public void Test_PointOnLine_vertical_line()
        {
            var line1 = new MyStraightLine(0, new MyPoint(10, 10), new MyPoint(10, 20));
            Assert.IsTrue(Mathematics.pointOnLine(10, 11, line1));
            Assert.IsTrue(Mathematics.pointOnLine(9.99, 13.01, line1));
            Assert.IsTrue(Mathematics.pointOnLine(10, 10, line1));

            Assert.IsFalse(Mathematics.pointOnLine(11, 11, line1));
            Assert.IsFalse(Mathematics.pointOnLine(9, 12, line1));
            Assert.IsFalse(Mathematics.pointOnLine(10, 9.9, line1));
            Assert.IsFalse(Mathematics.pointOnLine(10, 20.01, line1));
        }

        [TestMethod]
        public void Test_PointOnLine_extreme_slope()
        {
            var line1 = new MyStraightLine(0, new MyPoint(0, 0), new MyPoint(0.1, 1000));
            Assert.IsTrue(Mathematics.pointOnLine(0.05, 500, line1));
            Assert.IsTrue(Mathematics.pointOnLine(0.01, 500, line1));
            Assert.IsTrue(Mathematics.pointOnLine(0.99, 999, line1));

            Assert.IsFalse(Mathematics.pointOnLine(-1, 100, line1));
            Assert.IsFalse(Mathematics.pointOnLine(1, 500, line1, 0.000001));
            Assert.IsFalse(Mathematics.pointOnLine(-0.01, -100, line1));
            Assert.IsFalse(Mathematics.pointOnLine(0.2, 2000, line1));
        }
    }
}
