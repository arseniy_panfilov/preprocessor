{*********************************************************************}
{                                                                     }
{                    ���������� ����������� ��������                  }
{                                                                     }
{                               ������� 609                           }
{                                                                     }
{                         ����� ���� ������� 2008                     }
{                                                                     }
{*********************************************************************}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ResFunc, ExtCtrls, Buttons, CheckLst, Mask;

type
     MyString=PChar;
// ������(�����)
     TModule = Record
               Name: String;
           FullPath: String;
            Default: String;
            Current: String;
     end;
// ����� ��� ��������
     TFiles = Record
         RESULT1BIN: String;
         RESULT2BIN: String;
        GRIDDMINOUT: String;
    ProjectFormFile: String;
        NetVariants: String;
     end;
// ������
     TPluginReg = Record
      PluginRegName: String;
      PluginRegPath: String;
           SigmaReg: String;
              Files: TFiles;
            AlgRegF: TModule;
     end;
  TMainForm = class(TForm)
    INFOGroupBox: TGroupBox;
    Nodes: TMemo;
    ProgressBar: TProgressBar;
    Criteria: TGroupBox;
    Params: TPageControl;
    BaseAlg: TTabSheet;
    BaseMinTR: TGroupBox;
    Label2: TLabel;
    UseCheckBaseMinTR: TCheckBox;
    UpDownBaseMinTrunc: TUpDown;
    BaseMinTrunc: TEdit;
    BtnFillBaseMinTrunc: TButton;
    ForMinTr: TEdit;
    BaseMaxSum: TGroupBox;
    Label3: TLabel;
    UseBaseMaxS: TCheckBox;
    UpDownBaseMaxS: TUpDown;
    BaseMaxS: TEdit;
    BtnFillBaseMaxS: TButton;
    MaxS1: TEdit;
    BasePosition: TGroupBox;
    BasePosRough: TRadioButton;
    BasePosGravityCenter: TRadioButton;
    BasePosDoNotUse: TRadioButton;
    Addings: TTabSheet;
    CheckMinTrInNewElements: TGroupBox;
    Label4: TLabel;
    BaseCheckTRInNewElements: TCheckBox;
    UpDownBaseMinTrInNewElement: TUpDown;
    BaseMinTrInNewElement: TEdit;
    BtnFillBaseMinTrInNewElement: TButton;
    OutMinTrT: TEdit;
    CheckMinTrInNewSElements: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    BaseCheckTRInNewSElements: TCheckBox;
    UpDownBaseMinTrInNewSElement: TUpDown;
    BaseMinTrInNewSElement: TEdit;
    BaseCheckMinSInNewSElements: TCheckBox;
    UpDownBaseMinSInNewSElement: TUpDown;
    BaseMinSInNewSElement: TEdit;
    BtnFillBaseCheckTRInNewSElements: TButton;
    BtnFillBaseCheckMinSInNewSElements: TButton;
    OutMinTrS: TEdit;
    OutMinS: TEdit;
    ChangeD: TTabSheet;
    Posiotion: TGroupBox;
    PosRough: TRadioButton;
    PosGravityCenter: TRadioButton;
    PosDoNotUse: TRadioButton;
    CheckTR: TGroupBox;
    Label1: TLabel;
    UseMinTR: TCheckBox;
    UpDownMinTrunc: TUpDown;
    MinTrunc: TEdit;
    BtnFillMinTrunc: TButton;
    MinTrForDiag: TEdit;
    MeshNRS: TTabSheet;
    NRS_TODO: TGroupBox;
    NRS_ChangeD: TRadioButton;
    NRS_AppNewNode: TRadioButton;
    NRS_Position: TGroupBox;
    NRS_PosRough: TRadioButton;
    NRS_PosGravityCenter: TRadioButton;
    NRS_PosDoNotUse: TRadioButton;
    NRS_PosNewNode: TGroupBox;
    NRS_Middle: TRadioButton;
    NRS_Optimum: TRadioButton;
    Operations: TGroupBox;
    ToDo: TCheckListBox;
    DeleleDo: TButton;
    UP: TBitBtn;
    DOWN: TBitBtn;
    GroupBox1: TGroupBox;
    SaveIfAlgWas: TButton;
    Calculate: TButton;
    AddDo: TButton;
    ListOther: TListBox;
    ListMain: TListBox;
    Label7: TLabel;
    Label8: TLabel;
    INFO: TLabel;
    INFO2: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ShowForm(Sender: TObject);
    procedure ExitClick(Sender: TObject);
    procedure AddDoClick(Sender: TObject);
    procedure DeleleDoClick(Sender: TObject);
    procedure CalculateClick(Sender: TObject);
    procedure ToDoClickCheck(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure UseMinTRClick(Sender: TObject);
    procedure NRS_ChangeDClick(Sender: TObject);
    procedure NRS_AppNewNodeClick(Sender: TObject);
    procedure ClearNodesClick(Sender: TObject);
    procedure UseCheckBaseMinTRClick(Sender: TObject);
    procedure UseBaseMaxSClick(Sender: TObject);
    procedure BaseCalculateMaxSClick(Sender: TObject);
    procedure StopClick(Sender: TObject);
    procedure STARTClick(Sender: TObject);
    procedure BaseCheckTRInNewElementsClick(Sender: TObject);
    procedure BaseCheckTRInNewSElementsClick(Sender: TObject);
    procedure BaseCheckMinSInNewSElementsClick(Sender: TObject);
    procedure BtnFillBaseCheckMinSInNewSElementsClick(Sender: TObject);
    procedure BtnFillBaseCheckTRInNewSElementsClick(Sender: TObject);
    procedure BtnFillBaseMinTruncClick(Sender: TObject);
    procedure BtnFillBaseMaxSClick(Sender: TObject);
    procedure BtnFillBaseMinTrInNewElementClick(Sender: TObject);
    procedure BtnFillMinTruncClick(Sender: TObject);
//    procedure TabSheet1Show(Sender: TObject);
    procedure UPClick(Sender: TObject);
    procedure DOWNClick(Sender: TObject);
    procedure ListMainClick(Sender: TObject);
    procedure ListOtherClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SaveIfAlgWasClick(Sender: TObject);

  private
    { Private declarations }
    procedure Regularization;
    procedure BlockedNodesToForm;
    procedure BlockedNodesDefault;
//    procedure ShowTR;
    procedure ChangeDiagLines(TypeOfPos : Integer);stdcall;
    procedure PositionNodeToCenter(Node: Integer; TypeOfPos : Integer);stdcall;
    procedure PositionNodes(RGO: Integer;TypeOfPos : Integer);stdcall;
    procedure TDivision(TypeOfPos:Integer);
      Function CheckENeedDivision(ELEMENT:integer):boolean;
      Function CheckDoTDivision(ELEMENT:integer):integer;
      procedure DoTDivision(ELEMENT:integer;NEARELEMENT:integer);
      Function CalcSForSegment(ELEMENT:integer):real;
      Function CalcRoundSForSegment(ELEMENT:integer;CHECKEL:Integer; Log:  boolean):real;
      function CheckMinTrInElement(ELEMENT:Integer): boolean;
      function CheckMinTrInNewElement(ELEMENT:integer;NEARELEMENT:integer;Fx,Fy:Real; Treg: Integer): boolean;
      Function CheckMaxS(ELEMENT:integer):boolean;
    procedure SDivision(TypeOfPos:Integer);
      Function CheckForRSegment(ELEMENT : Integer):Boolean;
      Function CalcSForSegmentInCoord(x1,y1,x2,y2,x3,y3:real):real;          
    procedure SDivision2(TypeOfPos:Integer);
      Function CheckForRSegment2(ELEMENT : Integer):Boolean;
  public
    ActiveForm: Array [0..6] of Integer; // 1 - ShowCells, 2 - AlgF, 3 - Segment, 4 -, 5- SysSettings
    Function CalculateMinimumTr(Log: Boolean):Real;
    Function CalculateMaximumS(Log: Boolean):Real;
    Function CalculateMinimumS(Log: Boolean):Real;
    { Public declarations }
  end;

  TMyThread = class(TThread)
  private
  protected
    procedure Execute; override;
  public
    constructor Create(CreateSuspended: Boolean);
  end;

  // �������� ������ � ���������� ��������
  TProcPage = Record
    ChD: boolean; // �������� ��������� ����� ���������� (��������� ��� ���������)
    Pr1: boolean; // �������� ��������� ����������� (��������� ��� ���������)
    Pr2: boolean; // �������� ��������� ����������� (��������� ��� ���������)
  end;
// �������� ���������
  TOneProcedure = Record
    InTr  : Integer;
    InS   : Integer;
    OutTr : Integer;
    OutS  : Integer;
    Pos   : Integer; // (0 - ��� �����., 1 - ������ �����., 2 - �� ������ ������� )
    Pages : TProcPage;
  end;
// ������ ��������
  TProcedures = Record
    Proc  : array of TOneProcedure;
    count : Integer;
  end;

var

// Main
         AlgF: TMainForm;
     PluginReg: TPluginReg;
  State   : Boolean;
  Rx,Ry   : Real;
  ProcList: TProcedures;
  Thread  : TMyThread;
implementation

USES Math, Registry;

{$R *.dfm}

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  AlgF:=nil;
  Upload.free;
END;

procedure TMainForm.FormShow(Sender: TObject);
var
  Registry:TRegistry;

BEGIN

  if ErrorM then begin
    AlgF.Close;
    AlgF := nil;
  end;

  Registry:=TRegistry.Create;
  Registry.RootKey:=HKEY_CURRENT_USER;

  Update;
  Updating;
end;

procedure TMainForm.ShowForm(Sender: TObject);
BEGIN
  Show;
end;

procedure TMainForm.ExitClick(Sender: TObject);
begin
  AlgF.Close;
  AlgF := nil;
end;

procedure TMainForm.FormCreate(Sender: TObject);
Var
  projectDir : string;
begin
  projectDir := ParamStr(1);
//  projectFile := '';
  List.ResNodes.CountOfBlocked:=0;
  List.ResNodes.CountOfUnBlocked:=0;
// ����� �������
  PluginReg.PluginRegName:='RupertGraph';
 // PluginReg.SigmaReg:=StringReg+'\';
  PluginReg.PluginRegPath:=PluginReg.SigmaReg+PluginReg.PluginRegName+'\';
  // ShowFormReg
{  PluginReg.ShowFormReg.Name:='ShowRes';
  PluginReg.ShowFormReg.FullPath:=PluginReg.PluginRegPath+PluginReg.ShowFormReg.Name+'\';
  PluginReg.ShowFormReg.Default:=PluginReg.ShowFormReg.FullPath+'default\';
  PluginReg.ShowFormReg.Current:=PluginReg.ShowFormReg.FullPath+'current\';}
  // AlgRegF
  PluginReg.AlgRegF.Name:='AlgParams';
  PluginReg.AlgRegF.FullPath:=PluginReg.PluginRegPath+PluginReg.AlgRegF.Name+'\';
  PluginReg.AlgRegF.Default:=PluginReg.AlgRegF.FullPath+'default\';
  PluginReg.AlgRegF.Current:=PluginReg.AlgRegF.FullPath+'current\';

  // ���� � ������ �����������


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

  PluginReg.Files.RESULT1BIN:=projectDir+'\RESULT1.BIN';
  PluginReg.Files.RESULT2BIN:=projectDir+'\RESULT2.BIN';
//  PluginReg.Files.GRIDDMNODES:=ChangeFileExt(projectFile+'griddm', '.nodes');
//  PluginReg.Files.GRIDDMFINOUT:=ChangeFileExt(projectFile+'griddm', '.finout');
  PluginReg.Files.GRIDDMINOUT:=projectDir+'\griddm.inout';
//  PluginReg.Files.GRIDDMELEMS:=ChangeFileExt(projectFile+'griddm', '.elems');
// ������

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


//  PluginReg.Files.ProjectFormFile:=Project_GetFormFile;
  PluginReg.Files.NetVariants:=projectDir + '\grid.Ralg'; {���� ��������� ����� ��}

// ����������
  Upload:=TResFunc.Create;
  Upload.DoIT;

  {if ErrorM = true then begin
    AlgF.Close;
    AlgF := nil
  end;    }

  //Upload.CreateNewVariant;
  CountVar:=1;
//  Upload.CopyToVariant(CountVar+2);
//  CheckPoint.Items.Add('�������� �����');
//  CheckPoint.ItemIndex:=0;
//  MainVariant:=CountVar;
  //CountVar:=CheckPoint.Items.Count-1;

{  Upload.CreateNewVariant;
  Upload.CopyToVariant(0);
  Upload.SetNameOfVariant(0, '�������� �����');
  CountVar:=1; }

  ProcList.count:=0;
end;

// TMyThread
constructor TMyThread.Create(CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);
  Priority := tpIdle;
end;

procedure TMyThread.Execute;
begin
   AlgF.STARTClick(nil);
end;

// ��������� ��������
procedure TMainForm.AddDoClick(Sender: TObject);
var
  str,s2: string;
begin
  IF PosGravityCenter.Checked then str:=PosGravityCenter.Caption
    ELSE IF PosDoNotUse.Checked then str:=PosDoNotUse.Caption
    ELSE IF PosRough.Checked then str:=PosRough.Caption;
  IF BasePosGravityCenter.Checked then s2:=BasePosGravityCenter.Caption
    ELSE IF BasePosDoNotUse.Checked then s2:=BasePosDoNotUse.Caption
    ELSE IF BasePosRough.Checked then s2:=BasePosRough.Caption;
// ���������� � ������
  If ListMain.ItemIndex >= 0 then begin // ��������
    IF ListMain.Items.Strings[ListMain.ItemIndex]='����� ����������' then ToDo.Items.Add(ListMain.Items.Strings[ListMain.ItemIndex]+' ('+str+')')
    ELSE IF ListMain.Items.Strings[ListMain.ItemIndex]='��������� �����������' then ToDo.Items.Add(ListMain.Items.Strings[ListMain.ItemIndex]+' ('+s2+')')
    ELSE IF ListMain.Items.Strings[ListMain.ItemIndex]='��������� ��������' then ToDo.Items.Add(ListMain.Items.Strings[ListMain.ItemIndex]+' ('+s2+')')
    ELSE IF ListMain.Items.Strings[ListMain.ItemIndex]='��������� �������' then ToDo.Items.Add(ListMain.Items.Strings[ListMain.ItemIndex]+' ('+s2+')')
      else ToDo.Items.Add(ListMain.Items.Strings[ListMain.ItemIndex]);
    ListMain.ItemIndex:=-1;
  end;
  If ListOther.ItemIndex >= 0 then begin // ��������������
{    IF ListOther.Items.Strings[ListOther.ItemIndex]='���������������� �����' then ToDo.Items.Add(ListOther.Items.Strings[ListOther.ItemIndex]+' ('+str+')')
      else} ToDo.Items.Add(ListOther.Items.Strings[ListOther.ItemIndex]);
    ListOther.ItemIndex:=-1;
  end;
  ToDo.Checked[ToDo.Count-1] := True;
end;

// ������� ��������
procedure TMainForm.DeleleDoClick(Sender: TObject);
begin
  if ToDo.ItemIndex>=0 then begin
    ToDo.DeleteSelected;
  end;
end;
// �������� ����� �� ������
procedure TMainForm.UPClick(Sender: TObject);
begin
  if ToDo.ItemIndex>0 then Todo.Items.Exchange(ToDo.ItemIndex,ToDo.ItemIndex-1);
end;
// �������� ���� �� ������
procedure TMainForm.DOWNClick(Sender: TObject);
begin
  if ToDo.ItemIndex<(ToDo.Items.Count-1) then Todo.Items.Exchange(ToDo.ItemIndex,ToDo.ItemIndex+1);
end;

procedure TMainForm.STARTClick(Sender: TObject);
Var
  i: integer;
begin
//  HChDiag:=beginthread(nil,1024000,@TMainForm.ChangeDiagLines,nil,0,ChDiag);// ����� ��� ����������
  For i:=1 to ToDo.Items.Count do begin
    if ToDo.Checked[i-1] then begin
      IF ToDo.Items.Strings[i-1]='����� ���������� ('+PosGravityCenter.Caption+')' then ChangeDiagLines(2)
      else IF ToDo.Items.Strings[i-1]='����� ���������� ('+PosDoNotUse.Caption+')' then ChangeDiagLines(0)
      else IF ToDo.Items.Strings[i-1]='����� ���������� ('+PosRough.Caption+')' then ChangeDiagLines(1)
      else IF ToDo.Items.Strings[i-1]='��������� ����������� ('+PosGravityCenter.Caption+')' then TDivision(2)
      else IF ToDo.Items.Strings[i-1]='��������� ����������� ('+PosDoNotUse.Caption+')' then TDivision(0)
      else IF ToDo.Items.Strings[i-1]='��������� ����������� ('+PosRough.Caption+')' then TDivision(1)
      else IF ToDo.Items.Strings[i-1]='��������� �������� ('+PosGravityCenter.Caption+')' then SDivision(2)
      else IF ToDo.Items.Strings[i-1]='��������� �������� ('+PosDoNotUse.Caption+')' then SDivision(0)
      else IF ToDo.Items.Strings[i-1]='��������� �������� ('+PosRough.Caption+')' then SDivision(1)
      else IF ToDo.Items.Strings[i-1]='��������� ������� ('+PosGravityCenter.Caption+')' then SDivision2(2)
      else IF ToDo.Items.Strings[i-1]='��������� ������� ('+PosDoNotUse.Caption+')' then SDivision2(0)
      else IF ToDo.Items.Strings[i-1]='��������� ������� ('+PosRough.Caption+')' then SDivision2(1)
      else IF ToDo.Items.Strings[i-1]='������������� �����' then Regularization
      else IF ToDo.Items.Strings[i-1]='����������� �����' then BlockedNodesToForm
      else IF ToDo.Items.Strings[i-1]='�������������� �����' then BlockedNodesDefault
      else IF ToDo.Items.Strings[i-1]=('���������������� ����� ('+PosGravityCenter.Caption+')') then PositionNodes(1,2)
      else IF ToDo.Items.Strings[i-1]=('���������������� ����� ('+PosDoNotUse.Caption+')') then PositionNodes(1,0)
      else IF ToDo.Items.Strings[i-1]=('���������������� ����� ('+PosRough.Caption+')') then PositionNodes(1,1)
    else Nodes.Lines.Add('�� ����� ���������!');
    end;
  end;
//  terminatethread(HChDiag,0); // �������� ����� ��� ����������
end;

procedure TMainForm.CalculateClick(Sender: TObject);
Var
 i: Integer;
 Ch1: boolean;
begin
  SaveIfAlgWas.Visible:=False;
  Ch1:=False;
  Nodes.Lines.Clear;
  For i:=1 to ToDo.Items.Count do if ToDo.Checked[i-1] then Ch1:=True;
  IF Ch1 then begin
// ���������� ��������� �������� ������� ����������� ��������
    State:=True;
// ��������� ����� ��� �������
    Thread:=TMyThread.Create(True);
    Thread.Priority:=tpLower;
//  T2:=TMyThread.Create(True);
    Thread.Execute;
    Thread.Terminate;
//  STARTClick(nil);
//    Stop.Visible:=False;
    INFO.Visible:=False;
    ProgressBar.Position:=0;
//    Upload.CopyToVariant(MainVariant); {��������� ���������} - � ���� ��???
    SaveIfAlgWas.Visible:=True;
  end
  else Nodes.Lines.Add('<�� ������� ��������!>');
end;

procedure TMainForm.ToDoClickCheck(Sender: TObject);
begin
  ToDo.ItemIndex:=-1;
end;

// ������� ��������� � �������������� ��������
procedure TMainForm.ListMainClick(Sender: TObject);
begin
  ListOther.ItemIndex:=-1;
end;

// ������� ��������� � �������� ��������
procedure TMainForm.ListOtherClick(Sender: TObject);
begin
  ListMain.ItemIndex:=-1;
end;

 //        '����� ����������'
procedure TMainForm.ChangeDiagLines(TypeOfPos : Integer);
var
  I,Num_Diag,old: integer;
  Node1,Node2,Node3: integer; // ���� �������� � ���� �������� ��������
  ELEMENT, POS: INTEGER;
  function findNextNode(n1:Integer;n2:integer;k:integer):integer; // ����� ���� � ������� ��������, ��� �������� 2 ����, � ��� ����� ������� �� ����������
  var
    i2: integer;
  begin
    Result:=-1;
    for i2:=1 to list.ResElements.Count do begin
      if list.ResElements.Elements[i2].Node1=n1 then begin
          IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEMENT:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEMENT:=i2; POS:=2; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node2=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEMENT:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEMENT:=i2; POS:=1; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node3=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEMENT:=i2; POS:=2; Exit; end
          else IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEMENT:=i2; POS:=1; Exit; end;
      end
      else Result:=-1;
    end;
  end;
  function Dimension(x1,y1,x2,y2:real): real; // ���������� ����� ����� �������
    begin Result:=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); end;
  function CheckNeedChange(n1:integer;n2:integer;n3:integer;r:integer): boolean;
  Const
    pi=3.1415926535897932384626433832795;
  Var
    P1x,P1y,P2x,P2y: real; // ���������� ������������ ���������
    D1x,D1y,D2x,D2y: real; // ���������� ������� ���������
    P,D: real;    // ����� ����������
    tr1,tr2,tr3,tr4,tr5,tr6:real;    // ������������ ����
    Flag: boolean;          // ���� (���� ��� ���� ������ �������, �� True, ���� ���� �� 1 ������, �� False)
    s1,s2,s3,s4,sP,sD: real;
  begin
      P1x:=list.ResNodes.Nodes[Upload.Find(N3)].X;
      P1y:=list.ResNodes.Nodes[Upload.Find(N3)].Y;
      P2x:=list.ResNodes.Nodes[Upload.Find(R)].X;
      P2y:=list.ResNodes.Nodes[Upload.Find(R)].Y;
      D1x:=list.ResNodes.Nodes[Upload.Find(N1)].X;
      D1y:=list.ResNodes.Nodes[Upload.Find(N1)].Y;
      D2x:=list.ResNodes.Nodes[Upload.Find(N2)].X;
      D2y:=list.ResNodes.Nodes[Upload.Find(N2)].Y;
      P:=Dimension(P1x,P1y,P2x,P2y);
      D:=Dimension(D1x,D1y,D2x,D2y);
// ���� P<D, �� ������ �� ������� :)
      IF ((D-P)>0.0001) then begin
        IF UseMinTR.Checked then begin
          Flag:=True;
        // �������� ������������ �����
          tr1:=ABS(ArcTan2((D1x-P1x)*(P2y-P1y)-(P2x-P1x)*(D1y-P1y),(D1x-P1x)*(P2x-P1x)+(D1y-P1y)*(P2y-P1y))*(180/pi));
          tr2:=ABS(ArcTan2((P1x-D1x)*(P2y-D1y)-(P2x-D1x)*(P1y-D1y),(P1x-D1x)*(P2x-D1x)+(P1y-D1y)*(P2y-D1y))*(180/pi));
          tr3:=ABS(180-(tr1+tr2));
          tr4:=ABS(ArcTan2((D2x-P1x)*(P2y-P1y)-(P2x-P1x)*(D2y-P1y),(D2x-P1x)*(P2x-P1x)+(D2y-P1y)*(P2y-P1y))*(180/pi));
          tr5:=ABS(ArcTan2((P1x-D2x)*(P2y-D2y)-(P2x-D2x)*(P1y-D2y),(P1x-D2x)*(P2x-D2x)+(P1y-D2y)*(P2y-D2y))*(180/pi));
          tr6:=ABS(180-(tr4+tr5));
      // �������� ������������ ����
          IF tr1<StrToInt(MinTrunc.Text) then begin Flag:=False;        end
            ELSE IF tr2<StrToInt(MinTrunc.Text) then begin Flag:=False; end
            ELSE IF tr3<StrToInt(MinTrunc.Text) then begin Flag:=False; end
            ELSE IF tr4<StrToInt(MinTrunc.Text) then begin Flag:=False; end
            ELSE IF tr5<StrToInt(MinTrunc.Text) then begin Flag:=False; end
            ELSE IF tr6<StrToInt(MinTrunc.Text) then begin Flag:=False; end;

          IF Flag then
            begin
              // �������� ������ ���������
              s1:= CalcSForSegmentInCoord(d1x,d1y,d2x,d2y,p1x,p1y);
              s2:= CalcSForSegmentInCoord(d1x,d1y,d2x,d2y,p2x,p2y);
              sD:=s1+s2;
              s3:= CalcSForSegmentInCoord(p1x,p1y,p2x,p2y,d1x,d1y);
              s4:= CalcSForSegmentInCoord(p1x,p1y,p2x,p2y,d2x,d2y);
              sP:=s3+s4;
              IF (s3<0.1) or (s4<0.1) then Result:=False
              else if (ABS(sD-sP)>0.0001) then begin Result:=False; end
              else Result:=True;
            end
            else Result:=False;
        end
        else
          begin
            // �������� ������ ���������
            s1:= CalcSForSegmentInCoord(d1x,d1y,d2x,d2y,p1x,p1y);
            s2:= CalcSForSegmentInCoord(d1x,d1y,d2x,d2y,p2x,p2y);
            sD:=s1+s2;
            s3:= CalcSForSegmentInCoord(p1x,p1y,p2x,p2y,d1x,d1y);
            s4:= CalcSForSegmentInCoord(p1x,p1y,p2x,p2y,d2x,d2y);
            sP:=s3+s4;
            IF (s3<0.1) or (s4<0.1) then Result:=False
            else if (ABS(sD-sP)>0.1) then begin Result:=False; end
            else Result:=True;
          end
      end
      else Result:=False;
  end;
  procedure ChangeIFNeeded(n1:integer;n2:integer;n3:integer);
  Var
    NodeR: Integer;
  begin
  IF ((not list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut) and (not list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut)) or
    (List.ResElements.Elements[i].Material = List.ResElements.Elements[ELEMENT].Material) then begin
    NodeR:=findNextNode(N1,N2,N3);
    IF NodeR>0 then begin
      IF CheckNeedChange(n1,n2,n3, NodeR) then begin
        list.ResElements.Elements[i].Node1:=NodeR;
        list.ResElements.Elements[i].Node2:=N2;
        list.ResElements.Elements[i].Node3:=N3;
        list.ResElements.Elements[ELEMENT].Node1:=NodeR;
        list.ResElements.Elements[ELEMENT].Node2:=N1;
        list.ResElements.Elements[ELEMENT].Node3:=N3;
        Nodes.Lines.Add(format('��������� ����� %d:%d ������ �� %d:%d',[Upload.Find(N1),Upload.Find(N2),Upload.Find(N3),Upload.Find(NodeR)]));
        Nodes.Update;
        inc(Num_Diag);
      END;
    end;
  end;  
  end;
begin
  Num_Diag:=0;
  Nodes.Lines.Add('����� ����������');
  Nodes.Update;
  INFO.Visible := True;
  ProgressBar.Position:=0;
  ProgressBar.Min:=0;
  ProgressBar.Max:=list.ResElements.Count;
  for i:=1 to list.ResElements.Count do begin
    IF not State then begin
      Nodes.Lines.Add('<- ���������� ��������->');
      Nodes.Lines.Add(format('���������� ���������� ����������: %d',[Num_Diag]));
      exit; end;
    Application.ProcessMessages; {��� ������}
    INFO.Caption:=format('�� %d ��������� ���������� %d',[list.ResElements.Count,i]);
    INFO.update;
// Node1-Node2
    Node1:=list.ResElements.Elements[i].Node1;
    Node2:=list.ResElements.Elements[i].Node2;
    Node3:=list.ResElements.Elements[i].Node3;
    ChangeIFNeeded(Node1,Node2,Node3); old:=Num_Diag;
    If Old<>Num_Diag then PositionNodes(0,TypeOfPos); // ������������� ����
// Node2-Node3
// 1<2,2<3,3<1
    Node1:=list.ResElements.Elements[i].Node1;
    Node2:=list.ResElements.Elements[i].Node2;
    Node3:=list.ResElements.Elements[i].Node3;
    ChangeIFNeeded(Node2,Node3,Node1);
    If Old<>Num_Diag then PositionNodes(0,TypeOfPos); // ������������� ����
// Node3-Node1
// 1<1,2<3,3<2
    Node1:=list.ResElements.Elements[i].Node1;
    Node2:=list.ResElements.Elements[i].Node2;
    Node3:=list.ResElements.Elements[i].Node3;
    ChangeIFNeeded(Node1,Node3,Node2);
    If Old<>Num_Diag then PositionNodes(0,TypeOfPos); // ������������� ����
    ProgressBar.Position:=ProgressBar.Position+1;
//    ProgressBar.Update;
  end;
  ProgressBar.Position:=0;
  INFO.Visible:=False;
  Nodes.Lines.Add(format('���������� ���������� ����������: %d',[Num_Diag]));
  Nodes.Lines.Add('--- ����� ���������� ��������� ---');
end;

//���������������� ���� ���������� ����� �� ������ ��������������
procedure TMainForm.PositionNodes(RGO: Integer; TypeOfPos : Integer);
  Procedure Start;
  begin
    IF RGO>0 then begin
      Nodes.Lines.Add('���������������� �����');
      Nodes.Update;
      INFO.Visible:=True;
      ProgressBar.Position:=0;
      ProgressBar.Min:=0;
      ProgressBar.Max:=list.ResNodes.Count;
    end
  end;
  Procedure Continue(i:integer);
  begin
    IF RGO>0 then begin
      INFO.Caption:=format('�� %d ����� ���������������� %d',[list.ResNodes.Count,i]);
      INFO.Update;
      ProgressBar.Position:=ProgressBar.Position+1;
      ProgressBar.Update;
      ProgressBar.Position:=0;
    end;
  end;
  Procedure Stop;
  begin
    IF RGO>0 then begin
      INFO.Visible:=False;
      Nodes.Lines.Add('--- ���� ����������������');
    end;
  end;
var
  i: integer;
begin
  IF TypeOfPos>0 then begin
//    HPosNodes:=beginthread(nil,1024,@TMainForm.PositionNodeToCenter,nil,0,PosNodes);// ����� ��� ����������������
    Start;
    for i:=1 to list.ResNodes.Count do begin
      if not list.ResNodes.Nodes[Upload.Find(i)].inf.InOut then PositionNodeToCenter(i,TypeOfPos);
      IF not State then begin Nodes.Lines.Add('<- ���������� ��������->'); exit; end;
      Application.ProcessMessages; {��� ������}
      Continue(i);
    end;
    Stop;
//    terminatethread(HPosNodes,0);// �������� ����� ��� ����������������
  end;
end;

//���������������� ����������� ���� �� ������ ��������
procedure TMainForm.PositionNodeToCenter(Node: Integer; TypeOfPos : Integer);
var
  i,ne,nn :integer; // �������, ���-�� ���������, �����
  s: array [0..50] of integer; // ��������� ������ ���������
  RoundE: array of integer; // ���������� ��������
  RoundNodes: array of integer; // ���������� ����
  CenterX,CenterY: real; // ����� ��������������
  function find(k: integer):integer; // ����� ���� � ������� ����� ��������, ��� �����
  begin
    if (list.ResElements.Elements[k].Node1=Node) or (list.ResElements.Elements[k].Node2=Node) or (list.ResElements.Elements[k].Node3=Node) then Result:=k
    else Result:=-1;
  end;
  function findNotAs(k: integer;R: integer):integer;// ����� ������������� � R ���� � k ��������
  begin
    if list.ResElements.Elements[k].Node1<>R then Result:=list.ResElements.Elements[k].Node1
    else if list.ResElements.Elements[k].Node2<>R then Result:=list.ResElements.Elements[k].Node2
    else if list.ResElements.Elements[k].Node3<>R then Result:=list.ResElements.Elements[k].Node3
    else Result:=-1;
  end;
  function findNextNode(k:Integer;k2:integer):integer; // ����� ��������� ���� � ��������, ��� �������� 2 ����, ��� ����� �� �� ����������
  var
    i2: integer;
  begin
    Result:=-1;
    for i2:=1 to ne do begin
      if list.ResElements.Elements[RoundE[i2]].Node1=Node then begin
          IF (list.ResElements.Elements[RoundE[i2]].Node2=k) and (list.ResElements.Elements[RoundE[i2]].Node3<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node3; Exit; end
          else IF (list.ResElements.Elements[RoundE[i2]].Node3=k) and (list.ResElements.Elements[RoundE[i2]].Node2<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node2; Exit; end;
      end
      else if list.ResElements.Elements[RoundE[i2]].Node2=Node then begin
          IF (list.ResElements.Elements[RoundE[i2]].Node1=k) and (list.ResElements.Elements[RoundE[i2]].Node3<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node3; Exit; end
          else IF (list.ResElements.Elements[RoundE[i2]].Node3=k) and (list.ResElements.Elements[RoundE[i2]].Node1<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node1; Exit; end;
      end
      else if list.ResElements.Elements[RoundE[i2]].Node3=Node then begin
          IF (list.ResElements.Elements[RoundE[i2]].Node1=k) and (list.ResElements.Elements[RoundE[i2]].Node2<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node2; Exit; end
          else IF (list.ResElements.Elements[RoundE[i2]].Node2=k) and (list.ResElements.Elements[RoundE[i2]].Node1<>k2) then begin Result:=list.ResElements.Elements[RoundE[i2]].Node1; Exit; end;
      end
      else Result:=-1;
    end;
  end;
  procedure PositionNodeToGroughCenter;
  var
    j: integer;
  begin
    // ���� ����� ��������������
    CenterX:=0; CenterY:=0;
    for j:=1 to nn do begin
      CenterX:= CenterX + List.ResNodes.Nodes[Upload.Find(RoundNodes[j])].X;
      CenterY:= CenterY + List.ResNodes.Nodes[Upload.Find(RoundNodes[j])].Y;
    end;
    IF nn<>0 then begin
      CenterX:=CenterX/nn;
      CenterY:=CenterY/nn;
      list.ResNodes.Nodes[Upload.Find(Node)].X:=CenterX;
      list.ResNodes.Nodes[Upload.Find(Node)].Y:=CenterY;
    end;
  end;
// ����� ������ ���� ��� �������������� � ������ ������ �� 20 (��� �������� ���������������, ��� ������ �� ��������)
  procedure PositionNodeToGravityCenter;
  var
    k: integer;
    Cx,Cy,Mx,My,Cs,ctxv,ctyv,Si: real; // ������� ������ + ������� ��������������
  begin
  // ���� ����� ��������������
    CenterX:=0; CenterY:=0;
    Mx:=0; My:=0; Si:=0; Cx:=0; Cy:=0;
    for k:=2 to nn do begin
      Cs:= ((List.ResNodes.Nodes[Upload.Find(RoundNodes[k-1])].X * List.ResNodes.Nodes[Upload.Find(RoundNodes[k])].Y)
         - (List.ResNodes.Nodes[Upload.Find(RoundNodes[k])].X * List.ResNodes.Nodes[Upload.Find(RoundNodes[k-1])].Y))/2;
      Cx:= (List.ResNodes.Nodes[Upload.Find(RoundNodes[k-1])].X + List.ResNodes.Nodes[Upload.Find(RoundNodes[k])].X)/3;
      Cy:= (List.ResNodes.Nodes[Upload.Find(RoundNodes[k-1])].Y + List.ResNodes.Nodes[Upload.Find(RoundNodes[k])].Y)/3;
      Mx:=Mx + Cs*Cx;
      My:=My + Cs*Cy;
      Si:=Si+Cs;
    end;
    IF Si<>0 then begin
      ctxv:=  Mx/Si;
      ctyv:=  My/Si;
      CenterX:=Round(ctxv*10)/10;
      CenterY:=Round(ctyv*10)/10;
      list.ResNodes.Nodes[Upload.Find(Node)].X:=CenterX;
      list.ResNodes.Nodes[Upload.Find(Node)].Y:=CenterY;
    end
    else begin
      CenterX:=Cx;
      CenterY:=Cy;
      list.ResNodes.Nodes[Upload.Find(Node)].X:=CenterX;
      list.ResNodes.Nodes[Upload.Find(Node)].Y:=CenterY;
    end;
  end;

begin
// ���� ���� ����������, �� ����� ������� ����� ����� ����� ���������
// +1 ������ ������ � �����, ����� ������� ������ (nn:=ne+1)
  if not list.ResNodes.Nodes[Upload.Find(Node)].inf.InOut then begin
    ne:=0;
    for I:=1 to list.ResElements.Count do begin
      if find(i)<>-1 then begin
        inc(ne);
        s[ne]:=i;
      end;
    end;
    nn:=ne+1;
    SetLength(RoundE,ne+1);
    SetLength(RoundNodes,nn+1);
// ���������� ������� ��������
    for I:=1 to ne do RoundE[i]:=s[i];
// ���������� ������� ���� (������� ������ ������� �������)
    for i:=1 to nn-1 do begin
      if i=1 then begin
        RoundNodes[i]:=findNotAs(RoundE[i],node);
        RoundNodes[i+1]:=findNextNode(RoundNodes[i],0);
      end
      else RoundNodes[i+1]:=findNextNode(RoundNodes[i],RoundNodes[i-1]);
    end;
   IF TypeOfPos = 1 then PositionNodeToGroughCenter
   ELSE IF TypeOfPos = 2 then PositionNodeToGravityCenter;
  end
//  else Memo1.Lines.Add('����:'+IntToStr(Upload.Find(Node))+'������ ��������');
end;

//        '��������� �����������'
procedure TMainForm.TDivision(TypeOfPos:Integer);
Var
  i, num_elem_old: integer;
  NeedDivision: boolean;
  CanDoDivision: integer;
  Done: Boolean;
  procedure DoIt;
  begin
    NeedDivision:=CheckENeedDivision(i); // �������� ������������� ���������
    IF not NeedDivision then CanDoDivision:=CheckDoTDivision(i); // �������� ����������� � ���������, ���� ��������
    IF CanDoDivision>0 then begin DoTDivision(i,CanDoDivision);
      Done:=True;
      PositionNodes(0,TypeOfPos); {������������� ����}
    End
    else Done:=False;
  end;
begin
  Nodes.Lines.Add('{ ��������� ������������');
  Nodes.Update;
  num_elem_old:=list.ResElements.Count;
  if (List.ResNodes.Count>0) and (List.ResElements.Count>0) then begin
    Done:=False;
    INFO.Visible:=True;
    ProgressBar.Min:=0;
// ���������� ��������� ���-�� ���� ������������ ������ ��� �������������
    Repeat
      i:=1;   CanDoDivision:=0;
      ProgressBar.Position:=0;
      ProgressBar.Max:=list.ResElements.Count;
      repeat
        IF not State then begin
          Nodes.Lines.Add('<- ���������� ��������->');
          Nodes.Lines.Add(format('���������� ������������� ���� %d',[num_elem_old]));
          Nodes.Lines.Add(format('����� ������������� ����� %d',[list.ResElements.Count]));
          exit; end;
        Application.ProcessMessages; {��� ������}
          INFO.Caption:=format('�� %d ��������� ���������� %d',[list.ResElements.Count,i]);
          INFO.update;
        // ��������� �� ������������ ���� � ������������
          IF UseCheckBaseMinTR.Checked and not(UseBaseMaxS.Checked) then
            IF CheckMinTrInElement(i) then DoIt;
        // ��������� �� ������������ �������
          IF (UseBaseMaxS.Checked)and not(UseCheckBaseMinTR.Checked) then
            IF CheckMaxS(i) then DoIt;
        // ��� �����������
          IF UseCheckBaseMinTR.Checked and UseBaseMaxS.Checked then
            IF CheckMinTrInElement(i) or CheckMaxS(i) then DoIt;
        // ��� �����������
          IF not(UseCheckBaseMinTR.Checked) and not(UseBaseMaxS.Checked) then DoIt;
        ProgressBar.Position:=ProgressBar.Position+1;
        inc(i);
      Until (i>list.ResElements.Count) or (CanDoDivision>0);
      IF (i>list.ResElements.Count) then  Done:=False;
    Until not Done;
    ProgressBar.Position:=0;
    INFO.Visible:=False;
  end;
  Nodes.Lines.Add(format('���������� ������������� ���� %d',[num_elem_old]));
  Nodes.Lines.Add(format('����� ������������� ����� %d',[list.ResElements.Count]));
  Nodes.Lines.Add(' ��������� ������������� ��������� ...}');
end;

procedure TMainForm.DoTDivision(ELEMENT:integer;NEARELEMENT:integer);
var
  m: integer;
  a: array [0..10] of integer;
  Function Unique(k:integer;Node:Integer): boolean;
  begin
    IF(List.ResElements.Elements[k].Node1<>Node) and
      (List.ResElements.Elements[k].Node2<>Node) and
      (List.ResElements.Elements[k].Node3<>Node) then Result:=True
    else Result:=False;
  end;
  function findNode(i2: integer;k1:integer;k2:integer):Integer; // ����� ��������� ���� � ��������, ��� �������� 2 ����, ��� ����� �� �� ����������
  begin
    if((List.ResElements.Elements[i2].Node1=k1) and (List.ResElements.Elements[i2].Node2=k2))
        or
      ((List.ResElements.Elements[i2].Node2=k1) and (List.ResElements.Elements[i2].Node1=k2))
        then Result:= list.ResElements.Elements[i2].Node3
    else if ((List.ResElements.Elements[i2].Node2=k1) and (List.ResElements.Elements[i2].Node3=k2))
              or
            ((List.ResElements.Elements[i2].Node3=k1) and (List.ResElements.Elements[i2].Node2=k2))
              then Result:= list.ResElements.Elements[i2].Node1
    else if ((List.ResElements.Elements[i2].Node1=k1) and (List.ResElements.Elements[i2].Node3=k2))
              or
            ((List.ResElements.Elements[i2].Node3=k1) and (List.ResElements.Elements[i2].Node1=k2))
              then Result:= list.ResElements.Elements[i2].Node2
    else Result:=-1;
  end;
  Procedure GetA;
  begin
    IF Unique(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node1) then begin
      a[1]:=List.ResElements.Elements[ELEMENT].Node1;
      a[2]:=List.ResElements.Elements[ELEMENT].Node2;
      a[4]:=List.ResElements.Elements[ELEMENT].Node3;
      a[3]:=findNode(NEARELEMENT,a[2],a[4]);
    end
    ELSE IF Unique(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node2) then begin
      a[1]:=List.ResElements.Elements[ELEMENT].Node2;
      a[2]:=List.ResElements.Elements[ELEMENT].Node3;
      a[4]:=List.ResElements.Elements[ELEMENT].Node1;
      a[3]:=findNode(NEARELEMENT,a[2],a[4]);
    end
    ELSE IF Unique(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node3) then begin
      a[1]:=List.ResElements.Elements[ELEMENT].Node3;
      a[2]:=List.ResElements.Elements[ELEMENT].Node2;
      a[4]:=List.ResElements.Elements[ELEMENT].Node1;
      a[3]:=findNode(NEARELEMENT,a[2],a[4]);
    end;
  end;
begin
  Nodes.Lines.Add(Format('��������� �������� %d',[ELEMENT]));
//  ������ � NEARELEMENT ������� ����� ���� � ��������� �� ������� ���������'
   CalcRoundSForSegment(ELEMENT,NEARELEMENT,False);
// ������� ����� ����
    List.ResNodes.Count:=List.ResNodes.Count+1;
    SetLength(List.ResNodes.Nodes,List.ResNodes.Count+1);
    List.ResNodes.Nodes[List.ResNodes.Count].Number:=List.ResNodes.Count;
    List.ResNodes.Nodes[List.ResNodes.Count].X:=Rx;
    List.ResNodes.Nodes[List.ResNodes.Count].Y:=Ry;
    List.ResNodes.Nodes[List.ResNodes.Count].inf.Blocked:=False;
    List.ResNodes.Nodes[List.ResNodes.Count].inf.InOut:=False;
// ������� ��� ����� ��������
    List.ResElements.Count:=List.ResElements.Count+2;
    SetLength(List.ResElements.Elements,List.ResElements.Count+1);
// ����������� ��������� ����
    GetA;
// ������
      List.ResElements.Elements[ELEMENT].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
      List.ResElements.Elements[ELEMENT].Node2:=a[1];
      List.ResElements.Elements[ELEMENT].Node3:=a[2];
      m:=List.ResElements.Elements[ELEMENT].Material;
      List.ResElements.Elements[NEARELEMENT].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
      List.ResElements.Elements[NEARELEMENT].Node2:=a[2];
      List.ResElements.Elements[NEARELEMENT].Node3:=a[3];
// �����
      List.ResElements.Elements[List.ResElements.Count-1].Number:=List.ResElements.Count-1;
      List.ResElements.Elements[List.ResElements.Count-1].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
      List.ResElements.Elements[List.ResElements.Count-1].Node2:=a[3];
      List.ResElements.Elements[List.ResElements.Count-1].Node3:=a[4];
      List.ResElements.Elements[List.ResElements.Count-1].Material:=m;
      List.ResElements.Elements[List.ResElements.Count].Number:=List.ResElements.Count;
      List.ResElements.Elements[List.ResElements.Count].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
      List.ResElements.Elements[List.ResElements.Count].Node2:=a[4];
      List.ResElements.Elements[List.ResElements.Count].Node3:=a[1];
      List.ResElements.Elements[List.ResElements.Count].Material:=m;
end;

// ���� �������� �������� �� True
Function TMainForm.CheckDoTDivision(ELEMENT:integer): integer;
var
  i,r: integer;
  Found: boolean; // �� ��� ���� �� 1 ������� ������
  Inside: boolean; // �� ��� �������� ������ ������ �� �� ���� ��������
  function finde(i2: integer;None:integer;k1:Integer;k2:integer):Boolean; // ����� ��������� ���� � ��������, ��� �������� 2 ����, ��� ����� �� �� ����������
  begin
      Result:=False;
      if list.ResElements.Elements[i2].Node1=None then begin
          IF (list.ResElements.Elements[i2].Node2=k1) and (list.ResElements.Elements[i2].Node3<>k2) then begin Result:=True; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=k1) and (list.ResElements.Elements[i2].Node2<>k2) then begin Result:=True; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node2=None then begin
          IF (list.ResElements.Elements[i2].Node1=k1) and (list.ResElements.Elements[i2].Node3<>k2) then begin Result:=True; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=k1) and (list.ResElements.Elements[i2].Node1<>k2) then begin Result:=True; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node3=None then begin
          IF (list.ResElements.Elements[i2].Node1=k1) and (list.ResElements.Elements[i2].Node2<>k2) then begin Result:=True; Exit; end
          else IF (list.ResElements.Elements[i2].Node2=k1) and (list.ResElements.Elements[i2].Node1<>k2) then begin Result:=True; Exit; end;
      end
      else Result:=False;
  end;
  function find(i2,n1,n2,n3:integer):boolean;
  begin  // ��������� ���� ��������  � ��������� ����������
      IF finde(i2,n1,n2,n3) and (not(list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut)) then Result:=True
      ELSE IF finde(i2,n1,n3,n2) and (not(list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut))  then Result:=True
      ELSE IF finde(i2,n2,n1,n3) and (not(list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut))  then Result:=True
      ELSE IF finde(i2,n2,n3,n1) and (not(list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut))  then Result:=True
      ELSE IF finde(i2,n3,n1,n2) and (not(list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut))  then Result:=True
      ELSE IF finde(i2,n3,n2,n1) and (not(list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut))  then Result:=True
      ElSE Result:=False;
  end;
begin
  Found:=False; Inside:=False;
// ��������� ������� �% ELEMENT ��� ���������
// ����� ������� ��������
  i:=0; r:=0;
  Repeat
// ��������� ���������� �� ����� Round � �����-���� � ������� ����� ����� ��������
    IF (i<>ELEMENT) then begin
      IF find(i,List.ResElements.Elements[Element].Node1,List.ResElements.Elements[Element].Node2,List.ResElements.Elements[Element].Node3) then begin
//        ��� �������� ELEMENT ������� ������� i
         Found:=True;
      end;
    end;
  Inc(i);
  Until ((i>=List.ResElements.Count){ or not(Inside)});
  IF Found then begin
    IF Inside then begin
      IF BaseCheckTRInNewElements.Checked then begin
        CalcRoundSForSegment(ELEMENT,ELEMENT,False);
        Found:=CheckMinTrInNewElement(ELEMENT,r,Rx,Ry,StrToInt(BaseMinTrInNewElement.Text));
        IF Found then Result:=r
        else begin
//        �� �������� �� ���.���� � ����������� ��������
          Result:=0;
        end;
      end
      else Result:=r;
    end
    Else begin
//      �� ������� ����� ���������� � ������� �������
        Result:=0;
    end;
  end
  ELSE begin
//  �� ������� ������� ���������
    Result:=0;
  end;
end;

Function TMainForm.CheckENeedDivision(ELEMENT:integer):boolean;
Var
  GravityS, RoundS: real;
begin
  Result:=False;
  if (List.ResNodes.Count>0) and (List.ResElements.Count>0) then begin
    INFO2.Visible:=True;
    INFO2.Caption:=format('& �������� �� ��������� ��� �������� %d',[ELEMENT]);
    INFO2.Update;
// ����� ������� �����������
    GravityS:=CalcSForSegment(ELEMENT);
// ����� ����� ��������� ����������
    RoundS:=CalcRoundSForSegment(ELEMENT,ELEMENT,False);
// ��������� ����� �� �� � ������������
    IF Abs(RoundS-GravityS)>1 then begin
    // Element ELEMENT ��� ������������ RoundS � ������� ���� GravityS
     Result:=False  //�� �����
    end
    else begin
//      ������ ������������
      Result:=True; // �����
    end;
    INFO2.Visible:=False;
  end;
end;

//        '��������� ���������'
procedure TMainForm.SDivision(TypeOfPos:Integer);
Var
  i, num_seg_old: integer;
  Done,Changed: Boolean;
begin
  Nodes.Lines.Add('{ ��������� ���������');
  Nodes.Update;
  INFO.Visible:=True;
  ProgressBar.Position:=0;
  ProgressBar.Min:=0;
  ProgressBar.Max:=list.ResElements.Count;
  num_seg_old:=list.ResElements.Count;
  Done:=True;
// ���������� ��������� ���-�� ���� ������������ ������ ��� �������������
    Repeat
      Nodes.Lines.Add(' ����� ����������� ��������...');
      i:=1;
      ProgressBar.Position:=0;
      ProgressBar.Max:=list.ResElements.Count;
      repeat
        IF not State then begin
          Nodes.Lines.Add('<- ���������� ��������->');
          Nodes.Lines.Add(format('���������� ��������� ���� %d',[num_seg_old]));
          Nodes.Lines.Add(format('����� ��������� ����� %d',[list.ResElements.Count]));
          exit; end;
        Application.ProcessMessages; {��� ������}
          INFO.Caption:=format('�� %d ��������� ���������� %d',[list.ResElements.Count,i]);
          INFO.update;
        // ���������
            IF CheckForRSegment(i) then begin
              Changed:=True;
              // ������������� ����
              PositionNodes(0,TypeOfPos);
            end  
            Else Changed:=False;
        ProgressBar.Position:=ProgressBar.Position+1;
        inc(i);
      Until (i>list.ResElements.Count) or Changed;
      IF (i>list.ResElements.Count) then Done:=False;
    Until not Done;
  ProgressBar.Position:=0;
  INFO.Visible:=False;
  Nodes.Lines.Add(format('���������� ��������� ���� %d',[num_seg_old]));
  Nodes.Lines.Add(format('����� ��������� ����� %d',[list.ResElements.Count]));
  Nodes.Lines.Add(' ��������� ��������� ��������� ...}');
end;

Function TMainForm.CheckForRSegment(ELEMENT : Integer): boolean;
var
  Node1,Node2,Node3: integer; // ���� �������� � ���� �������� ��������
  Found: boolean; // ���� ����� ���� ���� ���� ���������� �
  CenterX, CenterY: Real;
  ELEM, POS: INTEGER;
  function Dimension(x1,y1,x2,y2:real): real; // ���������� ����� ����� �������
    begin Result:=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); end;
  function findNextNode(n1:Integer;n2:integer;k:integer):integer; // ����� ���� � ������� ��������, ��� �������� 2 ����, � ��� ����� ������� �� ����������
  var
    i2: integer;
  begin
    Result:=-1;
    for i2:=1 to list.ResElements.Count do begin
      if list.ResElements.Elements[i2].Node1=n1 then begin
          IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEM:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEM:=i2; POS:=2; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node2=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEM:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEM:=i2; POS:=1; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node3=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEM:=i2; POS:=2; Exit; end
          else IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEM:=i2; POS:=1; Exit; end;
      end
      else Result:=-1;
    end;
  end;
  function CheckNeedChange(n1:integer;n2:integer;n3:integer;r:integer): boolean;
  Const
    pi=3.1415926535897932384626433832795;
  Var
    P1x,P1y,P2x,P2y: real; // ���������� ������������ ���������
    D1x,D1y,D2x,D2y: real; // ���������� ������� ���������
    P,D, MinS: real;    // ����� ����������
  begin
      Result:=False;
      P1x:=list.ResNodes.Nodes[Upload.Find(N3)].X;
      P1y:=list.ResNodes.Nodes[Upload.Find(N3)].Y;
      P2x:=list.ResNodes.Nodes[Upload.Find(R)].X;
      P2y:=list.ResNodes.Nodes[Upload.Find(R)].Y;
      D1x:=list.ResNodes.Nodes[Upload.Find(N1)].X;
      D1y:=list.ResNodes.Nodes[Upload.Find(N1)].Y;
      D2x:=list.ResNodes.Nodes[Upload.Find(N2)].X;
      D2y:=list.ResNodes.Nodes[Upload.Find(N2)].Y;
      D:=Dimension(D1x,D1y,D2x,D2y);
      CenterX:=(D1x+D2x)/2;
      CenterY:=(D1y+D2y)/2;
      P:=Dimension(P2x,P2y,CenterX,CenterY);
// ���� P<D, �� ���� �����:)
      IF (((D/2)-P)>0.0001) then begin
      //  ������:���������� =  (D/2): P
// ���� ����� ����������� �� ����, �� ���������
        IF BaseCheckTRInNewSElements.Checked and not(BaseCheckMinSInNewSElements.Checked) then begin
          IF CheckMinTrInNewElement(ELEMENT,ELEM,CenterX,CenterY,StrToInt(BaseMinTrInNewSElement.Text)) then begin
            MinS:=1;//CalculateMinimumS;
            IF (CalcSForSegmentInCoord(CenterX,CenterY,P2x,P2y,D1x,D1y)>MinS)  // �� �������
              and (CalcSForSegmentInCoord(CenterX,CenterY,D1x,D1y,P1x,P1y)>MinS) //����� ���������
              and (CalcSForSegmentInCoord(CenterX,CenterY,P1x,P1y,D2x,D2y)>MinS)
              and (CalcSForSegmentInCoord(CenterX,CenterY,D2x,D2y,P2x,P2y)>MinS) then Result:=True
            else Result:=False;
          end
          else Result:=False; end
// ���� ����� ����������� �� �������, �� ���������
        ELSE IF BaseCheckMinSInNewSElements.Checked and not(BaseCheckTRInNewSElements.Checked) then begin
          IF (CalcSForSegmentInCoord(CenterX,CenterY,P2x,P2y,D1x,D1y)>StrToInt(BaseMinSInNewSElement.Text))  // �� �������
            and (CalcSForSegmentInCoord(CenterX,CenterY,D1x,D1y,P1x,P1y)>StrToInt(BaseMinSInNewSElement.Text)) //����� ���������
            and (CalcSForSegmentInCoord(CenterX,CenterY,P1x,P1y,D2x,D2y)>StrToInt(BaseMinSInNewSElement.Text))
            and (CalcSForSegmentInCoord(CenterX,CenterY,D2x,D2y,P2x,P2y)>StrToInt(BaseMinSInNewSElement.Text)) then Result:=True;
        end
// ���� �������� ��� �����������
        ELSE IF BaseCheckMinSInNewSElements.Checked and BaseCheckTRInNewSElements.Checked then begin
          IF CheckMinTrInNewElement(ELEMENT,ELEM,CenterX,CenterY,StrToInt(BaseMinTrInNewSElement.Text)) then begin
            IF (CalcSForSegmentInCoord(CenterX,CenterY,P2x,P2y,D1x,D1y)>StrToInt(BaseMinSInNewSElement.Text))  // �� �������
              and (CalcSForSegmentInCoord(CenterX,CenterY,D1x,D1y,P1x,P1y)>StrToInt(BaseMinSInNewSElement.Text)) //����� ���������
              and (CalcSForSegmentInCoord(CenterX,CenterY,P1x,P1y,D2x,D2y)>StrToInt(BaseMinSInNewSElement.Text))
              and (CalcSForSegmentInCoord(CenterX,CenterY,D2x,D2y,P2x,P2y)>StrToInt(BaseMinSInNewSElement.Text)) then Result:=True
            else Result:=False;
         end
          else Result:=False;
        end
// ���� �� �������� ��� �����������
        Else begin
         MinS:=1;//CalculateMinimumS;
          IF (CalcSForSegmentInCoord(CenterX,CenterY,P2x,P2y,D1x,D1y)>MinS)  // �� �������
            and (CalcSForSegmentInCoord(CenterX,CenterY,D1x,D1y,P1x,P1y)>MinS) //����� ���������
            and (CalcSForSegmentInCoord(CenterX,CenterY,P1x,P1y,D2x,D2y)>MinS)
            and (CalcSForSegmentInCoord(CenterX,CenterY,D2x,D2y,P2x,P2y)>MinS) then Result:=True
          else Result:=False;
        end;
      end
      else begin
//      ������ ������ ����������  (D/2):P
        Result:=False;
      end;
  end;
  Function ChangeIFNeeded(n1:integer;n2:integer;n3:integer): boolean;
  Var
    NodeR,m1,m2: Integer;
  begin
    Result:=False;
    NodeR:=findNextNode(N1,N2,N3);
   IF (list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut or list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut)
      and not(list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut and list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut
            and list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut) then begin
    IF NodeR>0 then begin
      IF CheckNeedChange(n1,n2,n3, NodeR) then begin
        Nodes.Lines.Add(format('��������� �������� %d:%d',[Upload.Find(N1),Upload.Find(N2)]));

// ������� ����� ����
        List.ResNodes.Count:=List.ResNodes.Count+1;
        SetLength(List.ResNodes.Nodes,List.ResNodes.Count+1);
        List.ResNodes.Nodes[List.ResNodes.Count].Number:=List.ResNodes.Count;
        List.ResNodes.Nodes[List.ResNodes.Count].X:=Centerx;
        List.ResNodes.Nodes[List.ResNodes.Count].Y:=Centery;
        List.ResNodes.Nodes[List.ResNodes.Count].inf.Blocked:=False;
{ � ����������� �� ����� �������� ���������� ������� ����� ���� ��� ����������}
        IF ((list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut and list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut) and
        ((not(list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut) and not(list.ResNodes.Nodes[Upload.Find(NodeR)].inf.InOut))
          or (List.ResElements.Elements[ELEMENT].Material <> List.ResElements.Elements[ELEM].Material))) then
          begin  {Nodes.Lines.Add(format('N1+N2=�����, � N3 � NodeR ���. ���������:%d-%d',[List.ResElements.Elements[ELEMENT].Material,List.ResElements.Elements[ELEM].Material]));}
          List.ResNodes.Nodes[List.ResNodes.Count].inf.InOut:=True;
          end
        else List.ResNodes.Nodes[List.ResNodes.Count].inf.InOut:=False;
// ������� ��� ����� ��������
        List.ResElements.Count:=List.ResElements.Count+2;
        SetLength(List.ResElements.Elements,List.ResElements.Count+1);
        m1:=List.ResElements.Elements[ELEMENT].Material;
        m2:=List.ResElements.Elements[ELEM].Material;
// ������
        List.ResElements.Elements[ELEMENT].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
        List.ResElements.Elements[ELEMENT].Node2:=NodeR;
        List.ResElements.Elements[ELEMENT].Node3:=N1;
        List.ResElements.Elements[ELEMENT].Material:=m2;
        List.ResElements.Elements[ELEM].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
        List.ResElements.Elements[ELEM].Node2:=N1;
        List.ResElements.Elements[ELEM].Node3:=N3;
        List.ResElements.Elements[ELEM].Material:=m1;
// �����
        List.ResElements.Elements[List.ResElements.Count-1].Number:=List.ResElements.Count-1;
        List.ResElements.Elements[List.ResElements.Count-1].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
        List.ResElements.Elements[List.ResElements.Count-1].Node2:=N3;
        List.ResElements.Elements[List.ResElements.Count-1].Node3:=N2;
        List.ResElements.Elements[List.ResElements.Count-1].Material:=m1;
        List.ResElements.Elements[List.ResElements.Count].Number:=List.ResElements.Count;
        List.ResElements.Elements[List.ResElements.Count].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
        List.ResElements.Elements[List.ResElements.Count].Node2:=N2;
        List.ResElements.Elements[List.ResElements.Count].Node3:=NodeR;
        List.ResElements.Elements[List.ResElements.Count].Material:=m2;
        Result:=True;
      END
      else begin
        Result:=False;
      end;
    end
  end;
  end;
begin
// Node1-Node2
    Node1:=list.ResElements.Elements[ELEMENT].Node1;
    Node2:=list.ResElements.Elements[ELEMENT].Node2;
    Node3:=list.ResElements.Elements[ELEMENT].Node3;
    Found:=ChangeIFNeeded(Node1,Node2,Node3);
// Node2-Node3
// 1<2,2<3,3<1
    IF not Found then begin
      Node1:=list.ResElements.Elements[ELEMENT].Node1;
      Node2:=list.ResElements.Elements[ELEMENT].Node2;
      Node3:=list.ResElements.Elements[ELEMENT].Node3;
      Found:=ChangeIFNeeded(Node2,Node3,Node1);
// Node3-Node1
// 1<1,2<3,3<2
      IF not Found then begin
        Node1:=list.ResElements.Elements[ELEMENT].Node1;
        Node2:=list.ResElements.Elements[ELEMENT].Node2;
        Node3:=list.ResElements.Elements[ELEMENT].Node3;
        Found:=ChangeIFNeeded(Node1,Node3,Node2);
        IF Found then Result:=True
        else begin Result:=False; // �� ���� ���� �� �������� � ������ �� ������ ��������
        end;
      end
      else Result:=True;
    end
    else Result:=True;
end;


//        '��������� ������� '
procedure TMainForm.SDivision2(TypeOfPos:Integer);
Var
  i, num_seg_old: integer;
  Done,Changed: Boolean;
begin
  Nodes.Lines.Add('{ ��������� �������');
  INFO.Visible:=True;
  ProgressBar.Position:=0;
  ProgressBar.Min:=0;
  ProgressBar.Max:=list.ResElements.Count;
  num_seg_old:=list.ResElements.Count;
  Done:=True;
// ���������� ��������� ���-�� ���� ������������ ������ ��� �������������
    Repeat
      Nodes.Lines.Add(' ����� ����������� ��������...');;
      i:=1;
      ProgressBar.Position:=0;
      ProgressBar.Max:=list.ResElements.Count;
      repeat
        IF not State then begin
          Nodes.Lines.Add('<- ���������� ��������->');
          Nodes.Lines.Add(format('���������� ��������� ���� %d',[num_seg_old]));
          Nodes.Lines.Add(format('����� ��������� ����� %d',[list.ResElements.Count]));
          exit; end;
        Application.ProcessMessages; {��� ������}
          INFO.Caption:=format('�� %d ��������� ���������� %d',[list.ResElements.Count,i]);
          INFO.update;
        // ���������
            IF CheckForRSegment2(i) then begin
              Changed:=True;
              // ������������� ����
              PositionNodes(0,TypeOfPos);
            end  
            Else Changed:=False;
        ProgressBar.Position:=ProgressBar.Position+1;
        inc(i);
      Until (i>list.ResElements.Count) or Changed;
      IF (i>list.ResElements.Count) then Done:=False;
    Until not Done;
  ProgressBar.Position:=0;
  INFO.Visible:=False;
  Nodes.Lines.Add(format('���������� ��������� ���� %d',[num_seg_old]));
  Nodes.Lines.Add(format('����� ��������� ����� %d',[list.ResElements.Count]));
  Nodes.Lines.Add(' ��������� ������� ��������� ...}');
end;

Function TMainForm.CheckForRSegment2(ELEMENT : Integer): boolean;
var
  Node1,Node2,Node3: integer; // ���� �������� � ���� �������� ��������
  f1,f2,f3: integer;
  a: array [0..4] of integer;
  Found: boolean; // ���� ����� ���� ���� ���� ���������� �
  CenterX, CenterY: Real;
  ELEM, POS: INTEGER;
  function Dimension(x1,y1,x2,y2:real): real; // ���������� ����� ����� �������
    begin Result:=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); end;
  function findNextNode(n1:Integer;n2:integer;k:integer):integer; // ����� ���� � ������� ��������, ��� �������� 2 ����, � ��� ����� ������� �� ����������
  var
    i2: integer;
  begin
    Result:=-1;
    for i2:=1 to list.ResElements.Count do begin
      if list.ResElements.Elements[i2].Node1=n1 then begin
          IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEM:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEM:=i2; POS:=2; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node2=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node3<>k) then begin Result:=list.ResElements.Elements[i2].Node3; ELEM:=i2; POS:=3; Exit; end
          else IF (list.ResElements.Elements[i2].Node3=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEM:=i2; POS:=1; Exit; end;
      end
      else if list.ResElements.Elements[i2].Node3=n1 then begin
          IF (list.ResElements.Elements[i2].Node1=n2) and (list.ResElements.Elements[i2].Node2<>k) then begin Result:=list.ResElements.Elements[i2].Node2; ELEM:=i2; POS:=2; Exit; end
          else IF (list.ResElements.Elements[i2].Node2=n2) and (list.ResElements.Elements[i2].Node1<>k) then begin Result:=list.ResElements.Elements[i2].Node1; ELEM:=i2; POS:=1; Exit; end;
      end
      else Result:=-1;
    end;
  end;
  function CheckNeedChange(nn1:integer;nn2:integer;nn3:integer): boolean;
  Const
    pi=3.1415926535897932384626433832795;
  Var
    P1x,P1y: real; // ���������� ������������ ���������
    D1x,D1y,D2x,D2y: real; // ���������� ������� ���������
    P,D, MinS,Treg: real;    // ����� ����������
// �������� ������������ ����
    Function CheckMinInElem(x1,y1,x2,y2,x3,y3:real):  boolean;
    Const
      pi=3.1415926535897932384626433832795;
    var
      tr1,tr2,tr3:real;
    begin
    // ����� x1 y1 x2 y2 x3 y3
      tr1:=ABS(ArcTan2((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1),(x2-x1)*(x3-x1)+(y2-y1)*(y3-y1))*(180/pi));
      tr2:=ABS(ArcTan2((x1-x2)*(y3-y2)-(x3-x2)*(y1-y2),(x1-x2)*(x3-x2)+(y1-y2)*(y3-y2))*(180/pi));
      tr3:=ABS(180-(tr1+tr2));
    // ���� tr1,tr2,tr3
      IF tr1<Treg then Result:=False
        ELSE IF tr2<Treg then Result:=False
        ELSE IF tr3<Treg then Result:=False
      Else Result:=True;
    end;
  begin
      Treg:=StrToInt(BaseMinTrInNewSElement.Text);
      P1x:=list.ResNodes.Nodes[Upload.Find(nn3)].X;
      P1y:=list.ResNodes.Nodes[Upload.Find(nn3)].Y;
      D1x:=list.ResNodes.Nodes[Upload.Find(nn1)].X;
      D1y:=list.ResNodes.Nodes[Upload.Find(nn1)].Y;
      D2x:=list.ResNodes.Nodes[Upload.Find(nn2)].X;
      D2y:=list.ResNodes.Nodes[Upload.Find(nn2)].Y;
      D:=Dimension(D1x,D1y,D2x,D2y);
      CenterX:=(D1x+D2x)/2;
      CenterY:=(D1y+D2y)/2;
      P:=Dimension(P1x,P1y,CenterX,CenterY);
      MinS := 0;
// ���� P<D, �� ���� �����:)
      IF (((D/2)-P)>0.0001) then begin
// ���� ����� ����������� �� ����, �� ���������
        IF BaseCheckTRInNewSElements.Checked and not(BaseCheckMinSInNewSElements.Checked) then begin
          IF CheckMinInElem(d1x,d1y,p1x,p1y,CenterX,CenterY)
              and CheckMinInElem(p1x,p1y,d2x,d2y,CenterX,CenterY)then begin
            MinS:=1;//CalculateMinimumS;
            IF (CalcSForSegmentInCoord(d1x,d1y,p1x,p1y,CenterX,CenterY)>MinS)  // �� �������
              and (CalcSForSegmentInCoord(p1x,p1y,d2x,d2y,CenterX,CenterY)>MinS) then Result:=True
            else Result:=False;
          end
          else Result:=False; end
// ���� ����� ����������� �� �������, �� ���������
        ELSE IF BaseCheckMinSInNewSElements.Checked and not(BaseCheckTRInNewSElements.Checked) then begin
            IF (CalcSForSegmentInCoord(d1x,d1y,p1x,p1y,CenterX,CenterY)>MinS)  // �� �������
              and (CalcSForSegmentInCoord(p1x,p1y,d2x,d2y,CenterX,CenterY)>MinS) then Result:=True
          else Result:=False;
        end
// ���� �������� ��� �����������
        ELSE IF BaseCheckMinSInNewSElements.Checked and BaseCheckTRInNewSElements.Checked then begin
          IF CheckMinInElem(d1x,d1y,p1x,p1y,CenterX,CenterY)
              and CheckMinInElem(p1x,p1y,d2x,d2y,CenterX,CenterY)then begin
            IF (CalcSForSegmentInCoord(d1x,d1y,p1x,p1y,CenterX,CenterY)>MinS)  // �� �������
              and (CalcSForSegmentInCoord(p1x,p1y,d2x,d2y,CenterX,CenterY)>MinS) then Result:=True
            else Result:=False;
         end
          else Result:=False;
        end
// ���� �� �������� ��� �����������
        Else begin
         MinS:=1;//CalculateMinimumS;
            IF (CalcSForSegmentInCoord(d1x,d1y,p1x,p1y,CenterX,CenterY)>MinS)  // �� �������
              and (CalcSForSegmentInCoord(p1x,p1y,d2x,d2y,CenterX,CenterY)>MinS) then Result:=True
          else Result:=False;
        end;
      end
      else begin
      //   ������ ������ ����������  (D/2):P
        Result:=False;
      end;
  end;
  Function ChangeIFNeeded(n1:integer;n2:integer;n3:integer): boolean;
  Var
    NodeR: Integer;
  begin
    f1:=0; f2:=0; f3:=0;
    IF list.ResNodes.Nodes[Upload.Find(N1)].inf.InOut then inc(f1);
    IF list.ResNodes.Nodes[Upload.Find(N2)].inf.InOut then inc(f2);
    IF list.ResNodes.Nodes[Upload.Find(N3)].inf.InOut then inc(f3);
    IF (f1+f2+f3)=2 then begin
    // � �������� ELEMENT ��� ���� ����� �� ������e
      NodeR:=findNextNode(N1,N2,N3);
      IF NodeR<0 then begin
        IF f1 = 0 then begin
        //  � �������� ELEMENT ������� ������������: N2-N3
          a[1]:=N2; a[2]:=N3; a[3]:=N1;
        END
        ELSE IF f2 = 0 then begin
        // � �������� ELEMENT ������� ������������: N1-N2
          a[1]:=N1; a[2]:=N3; a[3]:=N2;
        END
        ELSE begin
        // � �������� ELEMENT ������� ������������: N1-N2
          a[1]:=N1; a[2]:=N2; a[3]:=N3;
        END;
        // ��������� ������� a1-a2 �������� ELEMENT
        IF CheckNeedChange(a[1],a[2],a[3]) then begin
         Nodes.Lines.Add(format('��������� �������� %d:%d',[Upload.Find(a[1]),Upload.Find(a[2])]));
// ������� ����� ����
          List.ResNodes.Count:=List.ResNodes.Count+1;
          SetLength(List.ResNodes.Nodes,List.ResNodes.Count+1);
          List.ResNodes.Nodes[List.ResNodes.Count].Number:=List.ResNodes.Count;
          List.ResNodes.Nodes[List.ResNodes.Count].X:=Centerx;
          List.ResNodes.Nodes[List.ResNodes.Count].Y:=Centery;
          List.ResNodes.Nodes[List.ResNodes.Count].inf.Blocked:=True;
          List.ResNodes.Nodes[List.ResNodes.Count].inf.InOut:=True;
// ������� ����� �������
          List.ResElements.Count:=List.ResElements.Count+1;
          SetLength(List.ResElements.Elements,List.ResElements.Count+1);
// ������
          List.ResElements.Elements[ELEMENT].Node1:=a[1];
          List.ResElements.Elements[ELEMENT].Node2:=List.ResNodes.Count;
          List.ResElements.Elements[ELEMENT].Node3:=a[3];
// �����
          List.ResElements.Elements[List.ResElements.Count].Number:=List.ResElements.Count;
          List.ResElements.Elements[List.ResElements.Count].Node1:=List.ResNodes.Nodes[List.ResNodes.Count].Number;
          List.ResElements.Elements[List.ResElements.Count].Node2:=a[3];
          List.ResElements.Elements[List.ResElements.Count].Node3:=a[2];
          List.ResElements.Elements[List.ResElements.Count].Material:=List.ResElements.Elements[ELEMENT].Material;
          Result:=True;
        END
        else begin Result:=False; // �� ������������� ������������
        end;
      end
      else begin Result:=False; 
      end;
    end
    else Result:=False;
  end;{����� ChangeIFNeeded}
begin
// Node1-Node2
    Node1:=list.ResElements.Elements[ELEMENT].Node1;
    Node2:=list.ResElements.Elements[ELEMENT].Node2;
    Node3:=list.ResElements.Elements[ELEMENT].Node3;
    Found:=ChangeIFNeeded(Node1,Node2,Node3);
// Node2-Node3
// 1<2,2<3,3<1
   IF not Found then begin
      Node1:=list.ResElements.Elements[ELEMENT].Node1;
      Node2:=list.ResElements.Elements[ELEMENT].Node2;
      Node3:=list.ResElements.Elements[ELEMENT].Node3;
      Found:=ChangeIFNeeded(Node2,Node3,Node1);
// Node3-Node1
// 1<1,2<3,3<2
      IF not Found then begin
        Node1:=list.ResElements.Elements[ELEMENT].Node1;
        Node2:=list.ResElements.Elements[ELEMENT].Node2;
        Node3:=list.ResElements.Elements[ELEMENT].Node3;
        Found:=ChangeIFNeeded(Node1,Node3,Node2);
        IF Found then Result:=True
          else begin Result:=False; // �� ���� ���� �� �������� � ������ �� ������ ��������
        end;
      end
      else Result:=True; 
    end
    else Result:=True;
end;

//        '������������� �����'
procedure TMainForm.Regularization;
begin
  Nodes.Lines.Add('������������� �����');
end;
//        '����������� �����'
procedure TMainForm.BlockedNodesToForm;
begin
  Nodes.Lines.Add('����������� �����');
end;
//        '�������������� �����'
procedure TMainForm.BlockedNodesDefault;
begin
  Nodes.Lines.Add('�������������� �����');
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  Width:=900;
  Height:=600;
  Top  :=100;
  Left :=0;//Screen.Width;
end;

// ������ ������������ �������
Function TMainForm.CalculateMaximumS(Log: Boolean):Real;
var
  i,e: integer;
  s,MaxS:real;
begin
  Result:=0;
  if (List.ResNodes.Count>0) and (List.ResElements.Count>0) then begin
    MaxS:=0;
    e:=1;
    for i:=1 to list.ResElements.Count do begin
      S:=CalcSForSegment(i);
      IF MaxS<S then begin MaxS:=S; e:=i; end;
    end;
    If Log then Nodes.Lines.Add(Format('# ������������ ������� � �������� %d � � �������� %g (~ %f)',[e,MaxS,MaxS]));
    Result:=MaxS;
  end;
end;

// ������  ����������� �������
Function TMainForm.CalculateMinimumS(Log: Boolean):Real;
var
  i,e: integer;
  s,MinS:real;
begin
  Result:=99999;
  if (List.ResNodes.Count>0) and (List.ResElements.Count>0) then begin
    MinS:=99999;
    e:=1;
    for i:=1 to list.ResElements.Count do begin
      S:=CalcSForSegment(i);
      IF MinS>S then begin MinS:=S; e:=i; end;
    end;
    If Log then Nodes.Lines.Add(Format('# ����������� ������� � �������� %d � � �������� %g (~ %f)',[e,MinS,MinS]));
    Result:=MinS;
  end;
end;

// ������ ������������ ����
Function TMainForm.CalculateMinimumTr(Log: Boolean):Real;
Const
  pi=3.1415926535897932384626433832795;
Var
  I,e: Integer;
  x1,y1,x2,y2,x3,y3:real;
  tr1,tr2,tr3,tr:real;
begin
  Result:=180;
  // ������� ������������ ���� �� ���� �����
  if (List.ResNodes.Count>0) and (List.ResElements.Count>0) then begin
    Tr:=180;
    e:=1;
    for i:=1 to list.ResElements.Count do begin
     x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node1)].X;
     y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node1)].Y;
     x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node2)].X;
     y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node2)].Y;
     x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node3)].X;
     y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[i].Node3)].Y;
     tr1:=ABS(ArcTan2((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1),(x2-x1)*(x3-x1)+(y2-y1)*(y3-y1))*(180/pi));
     tr2:=ABS(ArcTan2((x1-x2)*(y3-y2)-(x3-x2)*(y1-y2),(x1-x2)*(x3-x2)+(y1-y2)*(y3-y2))*(180/pi));
     tr3:=ABS(180-(tr1+tr2));
     IF Tr>tr1 then begin Tr:=Tr1; e:=i; end
     else if Tr>tr2 then begin Tr:=Tr2;  e:=i; end
     else if Tr>tr3 then begin Tr:=Tr3; e:=i; end;
    end;
    If Log then Nodes.Lines.Add(Format('# ����������� ���� � �������� %d � ��� �������� %g (~ %f)',[e,Tr,Tr]));
    Result:=Tr;
  end;
end;

// ��������� ���� � ��������
  function TMainForm.CheckMinTrInElement(ELEMENT:Integer): boolean;
  Const
    pi=3.1415926535897932384626433832795;
  var
    x1,y1,x2,y2,x3,y3:real;
    tr1,tr2,tr3:real;
  begin
     x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].X;
     y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].Y;
     x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].X;
     y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].Y;
     x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].X;
     y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].Y;
     tr1:=ABS(ArcTan2((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1),(x2-x1)*(x3-x1)+(y2-y1)*(y3-y1))*(180/pi));
     tr2:=ABS(ArcTan2((x1-x2)*(y3-y2)-(x3-x2)*(y1-y2),(x1-x2)*(x3-x2)+(y1-y2)*(y3-y2))*(180/pi));
     tr3:=ABS(180-(tr1+tr2));
// �������� ������������ ����
      IF tr1<StrToInt(BaseMinTrunc.Text) then Result:=True
        ELSE IF tr2<StrToInt(BaseMinTrunc.Text) then Result:=True
        ELSE IF tr3<StrToInt(BaseMinTrunc.Text) then Result:=True
      Else Result:=False;
  end;
// ��������� ���� � ��������
  function TMainForm.CheckMinTrInNewElement(ELEMENT:Integer;NEARELEMENT:Integer; Fx,Fy:Real; Treg: Integer): boolean;
  Var
    a: array [0..4] of integer;
    x,y: array [0..5] of real;
    Function Unique2(k:integer;Node:Integer): boolean;
    begin
      IF(List.ResElements.Elements[k].Node1<>Node) and
        (List.ResElements.Elements[k].Node2<>Node) and
        (List.ResElements.Elements[k].Node3<>Node) then Result:=True
      else Result:=False;
    end;
    function findNode2(i2: integer;k1:integer;k2:integer):Integer; // ����� ��������� ���� � ��������, ��� �������� 2 ����, ��� ����� �� �� ����������
    begin
    if((List.ResElements.Elements[i2].Node1=k1) and (List.ResElements.Elements[i2].Node2=k2))
        or
      ((List.ResElements.Elements[i2].Node2=k1) and (List.ResElements.Elements[i2].Node1=k2))
        then Result:= list.ResElements.Elements[i2].Node3
    else if ((List.ResElements.Elements[i2].Node2=k1) and (List.ResElements.Elements[i2].Node3=k2))
              or
            ((List.ResElements.Elements[i2].Node3=k1) and (List.ResElements.Elements[i2].Node2=k2))
              then Result:= list.ResElements.Elements[i2].Node1
    else if ((List.ResElements.Elements[i2].Node1=k1) and (List.ResElements.Elements[i2].Node3=k2))
              or
            ((List.ResElements.Elements[i2].Node3=k1) and (List.ResElements.Elements[i2].Node1=k2))
              then Result:= list.ResElements.Elements[i2].Node2
      else Result:=-1;
    end;
    Procedure GetA2;
    begin
      IF Unique2(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node1) then begin
        a[1]:=List.ResElements.Elements[ELEMENT].Node1;
        a[2]:=List.ResElements.Elements[ELEMENT].Node2;
        a[4]:=List.ResElements.Elements[ELEMENT].Node3;
        a[3]:=findNode2(NEARELEMENT,a[2],a[4]);
      end
      ELSE IF Unique2(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node2) then begin
        a[1]:=List.ResElements.Elements[ELEMENT].Node2;
        a[2]:=List.ResElements.Elements[ELEMENT].Node3;
        a[4]:=List.ResElements.Elements[ELEMENT].Node1;
        a[3]:=findNode2(NEARELEMENT,a[2],a[4]);
      end
      ELSE IF Unique2(NEARELEMENT,List.ResElements.Elements[ELEMENT].Node3) then begin
        a[1]:=List.ResElements.Elements[ELEMENT].Node3;
        a[2]:=List.ResElements.Elements[ELEMENT].Node2;
        a[4]:=List.ResElements.Elements[ELEMENT].Node1;
        a[3]:=findNode2(NEARELEMENT,a[2],a[4]);
      end;
    end;
    Function CheckMinInElem(x1,y1,x2,y2,x3,y3:real):  boolean;
    Const
      pi=3.1415926535897932384626433832795;
    var
      tr1,tr2,tr3:real;
    begin
      tr1:=ABS(ArcTan2((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1),(x2-x1)*(x3-x1)+(y2-y1)*(y3-y1))*(180/pi));
      tr2:=ABS(ArcTan2((x1-x2)*(y3-y2)-(x3-x2)*(y1-y2),(x1-x2)*(x3-x2)+(y1-y2)*(y3-y2))*(180/pi));
      tr3:=ABS(180-(tr1+tr2));
// �������� ������������ ����
      IF tr1<Treg then Result:=False
        ELSE IF tr2<Treg then Result:=False
        ELSE IF tr3<Treg then Result:=False
      Else Result:=True;
    end;
  begin
//    CalcRoundSForSegment(ELEMENT,ELEMENT,False);
    GetA2; // ����������� ��������� ����
    x[1]:=List.ResNodes.Nodes[Upload.Find(a[1])].X;
    y[1]:=List.ResNodes.Nodes[Upload.Find(a[1])].Y;
    x[2]:=List.ResNodes.Nodes[Upload.Find(a[2])].X;;
    y[2]:=List.ResNodes.Nodes[Upload.Find(a[2])].Y;;
    x[3]:=List.ResNodes.Nodes[Upload.Find(a[3])].X;;
    y[3]:=List.ResNodes.Nodes[Upload.Find(a[3])].Y;;
    x[4]:=List.ResNodes.Nodes[Upload.Find(a[4])].X;;
    y[4]:=List.ResNodes.Nodes[Upload.Find(a[4])].Y;;
//    x[5]:=Rx; y[5]:=Ry;
    x[5]:=Fx; y[5]:=Fy;
   IF CheckMinInElem(x[5],y[5],x[1],y[1],x[2],y[2])
        and CheckMinInElem(x[5],y[5],x[2],y[2],x[3],y[3])
        and CheckMinInElem(x[5],y[5],x[3],y[3],x[4],y[4])
        and CheckMinInElem(x[5],y[5],x[4],y[4],x[1],y[1]) then Result:=True
    else Result:=False;
  end;

// �������� �� ������ �� ������������ �������
  Function TMainForm.CheckMaxS(ELEMENT:integer):boolean;
  Var
    Trouble:double;
    x1,y1,x2,y2,x3,y3,a,b,c,p,s,R:real;
  begin
    x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].X;
    y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].Y;
    x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].X;
    y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].Y;
    x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].X;
    y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].Y;
    c:=sqrt((y1-y2)*(y1-y2)+(x1-x2)*(x1-x2));
    a:=sqrt((y2-y3)*(y2-y3)+(x2-x3)*(x2-x3));
    b:=sqrt((y1-y3)*(y1-y3)+(x1-x3)*(x1-x3));
    p:=(a+b+c)/2;
    Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
    IF (Trouble<0) then S:=0 else S:=sqrt(Trouble);
    R:=S;//Round(abs(S)*10)/10;
    IF R>StrToInt(BaseMaxS.text) then Result:=True
      Else Result:=False;
  end;

Function TMainForm.CalcSForSegmentInCoord(x1,y1,x2,y2,x3,y3:real):real;
Var
  Trouble:double;
  a,b,c,p,s:real;
begin
      c:=sqrt((y1-y2)*(y1-y2)+(x1-x2)*(x1-x2));
      a:=sqrt((y2-y3)*(y2-y3)+(x2-x3)*(x2-x3));
      b:=sqrt((y1-y3)*(y1-y3)+(x1-x3)*(x1-x3));
      p:=(a+b+c)/2;
      Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
      IF (Trouble<0) then S:=0 else S:=sqrt(Trouble);
      Result:=Round(abs(S)*10)/10;
end;

Function TMainForm.CalcSForSegment(ELEMENT:integer):real;
Var
  Trouble:double;
  x1,y1,x2,y2,x3,y3,a,b,c,p,s:real;
begin
      x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].X;
      y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].Y;
      x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].X;
      y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].Y;
      x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].X;
      y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].Y;
      c:=sqrt((y1-y2)*(y1-y2)+(x1-x2)*(x1-x2));
      a:=sqrt((y2-y3)*(y2-y3)+(x2-x3)*(x2-x3));
      b:=sqrt((y1-y3)*(y1-y3)+(x1-x3)*(x1-x3));
      p:=(a+b+c)/2;
      Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
      IF (Trouble<0) then S:=0 else S:=sqrt(Trouble);
      Result:=S;//Round(abs(S)*10)/10;
end;

// ���������� ������� ��������������, ��� ������ ����� - ����� ��������� ����������
Function TMainForm.CalcRoundSForSegment(ELEMENT:integer;CHECKEL:Integer; Log: boolean):real;
Var
  RoundX,RoundY, Trouble:Extended	;
  x1,y1,x2,y2,x3,y3,a,b,c,p,s,S1,S2,S3:Extended;
  procedure OverRoundCenter;
  Var
    Determ: real;
  begin
    Determ:=x1*y2+y1*x3-y2*x3-x1*y3-x2*y1+x2*y3;
    IF Determ<>0 then begin
    RoundY :=(-1/2)*((-x2*x2*x1-y2*y2*x1-x2*x3*x3-x1*x1*x3-y1*y1*x3+x1*x3*x3+
                      x2*x2*x3+y2*y2*x3+x2*x1*x1+x2*y1*y1+x1*y3*y3-x2*y3*y3)
                      /Determ);
    RoundX :=(1/2)*((-x1*x1*y3+x1*x1*y2-x3*x3*y2+y1*y1*y2+y1*y3*y3+y3*x2*x2-
                      y1*x2*x2-y1*y2*y2+y1*x3*x3-y3*y3*y2-y1*y1*y3+y3*y2*y2)
                      /Determ);
    Rx:=RoundX; Ry:=RoundY;
    end
    else begin
      RoundX:=(x1+x2+x3)/3;
      RoundY:=(y1+y2+y3)/3;
    end;
  end;
  begin
  RoundX:=0; RoundY:=0;
      x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].X;
      y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node1)].Y;
      x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].X;
      y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node2)].Y;
      x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].X;
      y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[ELEMENT].Node3)].Y;
      OverRoundCenter;
      x1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node1)].X;
      y1:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node1)].Y;
      x2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node2)].X;
      y2:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node2)].Y;
      x3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node3)].X;
      y3:=List.ResNodes.Nodes[Upload.Find(list.ResElements.Elements[CHECKEL].Node3)].Y;
      Trouble:=(Roundy-y2)*(Roundy-y2)+(Roundx-x2)*(Roundx-x2); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then c:=-1 else c:=sqrt(Trouble);
      Trouble:=(y2-y3)*(y2-y3)+(x2-x3)*(x2-x3); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then a:=-1 else a:=sqrt(Trouble);
      Trouble:=(Roundy-y3)*(Roundy-y3)+(Roundx-x3)*(Roundx-x3); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then b:=-1 else b:=sqrt(Trouble);
      IF (a=-1) or (b=-1) or (c=-1) then S1:=-1
      else begin
        p:=(a+b+c)/2;
        Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then S1:=0 else S1:=sqrt(Trouble);
      end;
      Trouble:=(y1-Roundy)*(y1-Roundy)+(x1-Roundx)*(x1-Roundx); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then c:=-1 else c:=sqrt(Trouble);
      Trouble:=(Roundy-y3)*(Roundy-y3)+(Roundx-x3)*(Roundx-x3); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then a:=-1 else a:=sqrt(Trouble);
      Trouble:=(y1-y3)*(y1-y3)+(x1-x3)*(x1-x3); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then b:=-1 else b:=sqrt(Trouble);
      IF (a=-1) or (b=-1) or (c=-1) then S2:=-1
      else begin
        p:=(a+b+c)/2;
        Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then S2:=0 else S2:=sqrt(Trouble);
      end;
      Trouble:=(y1-y2)*(y1-y2)+(x1-x2)*(x1-x2); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then c:=-1 else c:=sqrt(Trouble);
      Trouble:=(y2-Roundy)*(y2-Roundy)+(x2-Roundx)*(x2-Roundx); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then a:=-1 else a:=sqrt(Trouble);
      Trouble:=(y1-Roundy)*(y1-Roundy)+(x1-Roundx)*(x1-Roundx); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then b:=-1 else b:=sqrt(Trouble);
      IF (a=-1) or (b=-1) or (c=-1) then S3:=-1
      else begin
        p:=(a+b+c)/2;
        Trouble:=p*(p-a)*(p-b)*(p-c); {�������� ����������� "-" ��-�� ����� ���������}
        IF (Trouble<0) then S3:=0 else S3:=sqrt(Trouble);
      end;
      IF (S1=-1) or (S2=-1) or (S3=-1) then S:=-1
      else S:=S1+S2+S3;
      Result:=S;//Round(abs(S)*10)/10;
  end;

// ����� ����������
procedure TMainForm.UseMinTRClick(Sender: TObject);
begin
  IF UseMinTR.Checked then begin
    UpDownMinTrunc.Enabled:=True;
    MinTrunc.Enabled:=True;
    BtnFillMinTrunc.Enabled:=True;
  end
  else begin
    UpDownMinTrunc.Enabled:=False;
    MinTrunc.Enabled:=False;
    BtnFillMinTrunc.Enabled:=False;
  end;
end;

// ������������ �����������
procedure TMainForm.NRS_ChangeDClick(Sender: TObject);
begin
  IF NRS_ChangeD.Checked then begin
    NRS_PosNewNode.Enabled:=True;
    NRS_PosRough.Enabled:=True;
    NRS_PosGravityCenter.Enabled:=True;
    NRS_PosDoNotUse.Enabled:=True;
// ����� ������������
    NRS_PosNewNode.Enabled:=False;
    NRS_Middle.Enabled:=False;
    NRS_Optimum.Enabled:=False;
  end
  else begin
    NRS_PosNewNode.Enabled:=False;
    NRS_PosRough.Enabled:=False;
    NRS_PosGravityCenter.Enabled:=False;
    NRS_PosDoNotUse.Enabled:=False;
  end;
end;

procedure TMainForm.NRS_AppNewNodeClick(Sender: TObject);
begin
  IF NRS_AppNewNode.Checked then begin
    NRS_PosNewNode.Enabled:=True;
    NRS_Middle.Enabled:=True;
    NRS_Optimum.Enabled:=True;
// ����� ������������
    NRS_PosNewNode.Enabled:=False;
    NRS_PosRough.Enabled:=False;
    NRS_PosGravityCenter.Enabled:=False;
    NRS_PosDoNotUse.Enabled:=False;
  end
  else begin
    NRS_PosNewNode.Enabled:=False;
    NRS_Middle.Enabled:=False;
    NRS_Optimum.Enabled:=False;
  end;
end;


procedure TMainForm.ClearNodesClick(Sender: TObject);
  begin Nodes.Lines.Clear;  end;

// ������� �������� (����������� ����)
procedure TMainForm.UseCheckBaseMinTRClick(Sender: TObject);
begin
  IF UseCheckBaseMinTR.Checked then begin
    UpDownBaseMinTrunc.Enabled:=True;
    BaseMinTrunc.Enabled:=True;
    BtnFillBaseMinTrunc.Enabled:=True;
  end
  else begin
    UpDownBaseMinTrunc.Enabled:=False;
    BaseMinTrunc.Enabled:=False;
    BtnFillBaseMinTrunc.Enabled:=False;
  end;
end;
// ������� �������� (����������� �������)
procedure TMainForm.UseBaseMaxSClick(Sender: TObject);
begin
   IF UseBaseMaxS.Checked then begin
    BaseMaxS.Enabled:=True;
    UpDownBaseMaxS.Enabled:=True;
    BtnFillBaseMaxS.Enabled:=True;
  end
  else begin
    BaseMaxS.Enabled:=False;
    UpDownBaseMaxS.Enabled:=False;
    BtnFillBaseMaxS.Enabled:=False;
  end;
end;

procedure TMainForm.BaseCalculateMaxSClick(Sender: TObject);
begin
{  IF BaseCalculateMaxS.Checked then} UpDownBaseMaxS.Enabled:=False;
  BaseMaxS.Text:=IntToStr(Trunc(CalculateMaximumS(True)));
end;

procedure TMainForm.StopClick(Sender: TObject);
begin
    State:=False;
//    terminatethread(HChDiag,0); // �������� ����� ��� ����������
//    Thread.Terminate;
end;

procedure TMainForm.BaseCheckTRInNewElementsClick(Sender: TObject);
begin
  IF BaseCheckTRInNewElements.Checked then begin
    BaseMinTrInNewElement.Enabled:=True;
    UpDownBaseMinTrInNewElement.Enabled:=True;
    BtnFillBaseMinTrInNewElement.Enabled:=True;
  end
  else begin
    BaseMinTrInNewElement.Enabled:=False;
    UpDownBaseMinTrInNewElement.Enabled:=False;
    BtnFillBaseMinTrInNewElement.Enabled:=False;
  end;
end;

procedure TMainForm.BaseCheckTRInNewSElementsClick(Sender: TObject);
begin
  IF BaseCheckTRInNewSElements.Checked then begin
    BaseMinTrInNewSElement.Enabled:=True;
    UpDownBaseMinTrInNewSElement.Enabled:=True;
    BtnFillBaseCheckTRInNewSElements.Enabled:=True;
  end
  else begin
    BaseMinTrInNewSElement.Enabled:=False;
    UpDownBaseMinTrInNewSElement.Enabled:=False;
    BtnFillBaseCheckTRInNewSElements.Enabled:=False;
  end;
end;

procedure TMainForm.BaseCheckMinSInNewSElementsClick(Sender: TObject);
begin
  IF BaseCheckMinSInNewSElements.Checked then begin
    BaseMinSInNewSElement.Enabled:=True;
    UpDownBaseMinSInNewSElement.Enabled:=True;
    BtnFillBaseCheckMinSInNewSElements.Enabled:=True;
  end
  else begin
    BaseMinSInNewSElement.Enabled:=False;
    UpDownBaseMinSInNewSElement.Enabled:=False;
    BtnFillBaseCheckMinSInNewSElements.Enabled:=False;
  end;
end;

// ������ �������� ��������� ���������

procedure TMainForm.BtnFillBaseCheckMinSInNewSElementsClick(
  Sender: TObject);
var i: integer;
begin
  i:=Trunc(CalculateMinimumS(True));
  if i=0 then i:=1;
  OutMinS.Text:=IntToStr(i);
  BaseMinSInNewSElement.Text := IntToStr(i);  
end;

procedure TMainForm.BtnFillBaseCheckTRInNewSElementsClick(Sender: TObject);
  begin
    OutMinTrS.Text:=IntToStr(Trunc(CalculateMinimumTr(True)));
    BaseMinTrInNewSElement.Text := IntToStr(Trunc(CalculateMinimumTr(True)));
  end;

procedure TMainForm.BtnFillBaseMinTruncClick(Sender: TObject);
  begin
    ForMinTr.Text:=IntToStr(Trunc(CalculateMinimumTr(True)));
    BaseMinTrunc.Text := IntToStr(Trunc(CalculateMinimumTr(True)));
  end;

procedure TMainForm.BtnFillBaseMaxSClick(Sender: TObject);
  begin
    MaxS1.Text:=IntToStr(Trunc(CalculateMaximumS(True)));
    BaseMaxS.Text := IntToStr(Trunc(CalculateMaximumS(True)));
  end;

procedure TMainForm.BtnFillBaseMinTrInNewElementClick(Sender: TObject);
  begin
    OutMinTrT.Text:=IntToStr(Trunc(CalculateMinimumTr(True)));
    BaseMinTrInNewElement.Text := IntToStr(Trunc(CalculateMinimumTr(True)));
  end;

procedure TMainForm.BtnFillMinTruncClick(Sender: TObject);
  begin
    MinTrForDiag.Text:=IntToStr(Trunc(CalculateMinimumTr(True)));
    MinTrunc.Text := IntToStr(Trunc(CalculateMinimumTr(True)));
  end;

procedure TMainForm.SaveIfAlgWasClick(Sender: TObject);
begin
//  Upload.ReCalculate;
  SaveIfAlgWas.Visible:=False;

  Upload.CreateNewVariant;

  Upload.CopyToVariant(CountVar);
  Inc(CountVar);
  
  Upload.WriteVariants;

  AlgF.Close;
  AlgF := nil;
end;

end.
