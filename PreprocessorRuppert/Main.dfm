object MainForm: TMainForm
  Left = 253
  Top = 47
  BorderStyle = bsToolWindow
  Caption = #1047#1072#1076#1072#1085#1080#1077' '#1072#1083#1075#1086#1088#1080#1090#1084#1072' '#1080' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1077#1075#1086' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074
  ClientHeight = 512
  ClientWidth = 612
  Color = clBtnFace
  Constraints.MaxHeight = 550
  Constraints.MaxWidth = 628
  Constraints.MinHeight = 550
  Constraints.MinWidth = 628
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFF
    FFFFFFFFFFFFFFFFFFFFFFFCCCFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFFFCFFFFC
    CCFFFFFFFFFF00000FFFFCFFFFFCFFCFFFCFFFFFFFFF0FF00EEEECFFFFFCFCFF
    FFFEEEEEEEEE0FF00FFFFCFFFFFCFCFFFFFCFFFFFFFF0FF00FFFFFCFFFCFFCFF
    FFFCFFFFFFFF0FF00FFFFFFCECFFFFCFFFEFFFFFFFFF0FF00FFFFFFEFFFFFFFC
    CCEFFFFFFFFF0FF00FFFFFFEFFFFFFFFFFEFFFFFFFFF0FF00FFFFFEFFFFFFFFF
    FEFFFFFFFFFF0FF00FFFFFEFFFFFFFFFFEFFFFFFFFFF0FF00FFFFEFFFFFFFFFF
    FEFFFFFFFFFF0FF00FFFFEFFFFFFFFFFFEFFFFFFFFFF0FF00FFFEFFFFFFFFFFF
    FEFFFFFFFFFF0FF00FFFEFFFFFFFFFFFEFFFFFFFFFFF0FF00FFEFFFFFFFFFFFF
    EFFF000000000FF0000EFFFFFFFFFFFFFEF0000000000FF0000FFFFFFFFFFFFF
    FEFF000000000FF0000FFFFFFFFFFFFFFEFF0FFFFFFFFFFFFF0FFFFFFFFFFFFF
    FEFFF00000000000000FFFFFFFFFFFFFFFEFFF0000000000000FFFFFFFFFFFFF
    FFFEFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFEEEFFFFFCCCEFFFFFFFFFFFFFFFF
    FFFFFFFEEFFCFFFCFFFFFFFFFFFFFFFFFFFFFFFFFEEFFFFFCFFFFFFFFFFFFFFF
    FFFFFFFFFFCFFFFFCFFFFFFFFFFFFFFFFFFFFFFFFFCFFFFFCFFFFFFFFFFFFFFF
    FFFFFFFFFFFCFFFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCFFFFFFFFFFFFFFFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object INFOGroupBox: TGroupBox
    Left = 312
    Top = 285
    Width = 297
    Height = 228
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1093#1086#1076#1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1072#1083#1075#1086#1088#1080#1090#1084#1072
    TabOrder = 0
    object INFO: TLabel
      Left = 8
      Top = 192
      Width = 25
      Height = 13
      Caption = 'INFO'
      Visible = False
    end
    object INFO2: TLabel
      Left = 8
      Top = 208
      Width = 25
      Height = 13
      Caption = 'INFO'
      Visible = False
    end
    object Nodes: TMemo
      Left = 2
      Top = 15
      Width = 293
      Height = 146
      Align = alTop
      Ctl3D = False
      MaxLength = 99999
      ParentCtl3D = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      WantReturns = False
    end
    object ProgressBar: TProgressBar
      Left = 3
      Top = 163
      Width = 288
      Height = 25
      Step = 1
      TabOrder = 1
    end
  end
  object Criteria: TGroupBox
    Left = 2
    Top = 0
    Width = 305
    Height = 263
    Caption = #1047#1072#1076#1072#1085#1080#1077' '#1082#1088#1080#1090#1077#1088#1080#1077#1074' '#1076#1083#1103' '#1087#1088#1086#1094#1077#1076#1091#1088' '#1072#1083#1075#1086#1088#1080#1090#1084#1072
    TabOrder = 1
    object Params: TPageControl
      Left = 2
      Top = 15
      Width = 301
      Height = 242
      ActivePage = ChangeD
      MultiLine = True
      ParentShowHint = False
      ShowHint = False
      TabOrder = 0
      object BaseAlg: TTabSheet
        Caption = #1055#1077#1088#1074#1080#1095#1085#1099#1077' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103
        ImageIndex = 2
        object BaseMinTR: TGroupBox
          Left = 0
          Top = 0
          Width = 293
          Height = 51
          Align = alTop
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1084#1080#1085#1080#1084#1072#1083#1100#1085#1086#1075#1086' '#1091#1075#1083#1072
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
          object Label2: TLabel
            Left = 121
            Top = 23
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object UseCheckBaseMinTR: TCheckBox
            Left = 8
            Top = 22
            Width = 113
            Height = 17
            Caption = #1044#1077#1083#1072#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
            TabOrder = 0
            OnClick = UseCheckBaseMinTRClick
          end
          object UpDownBaseMinTrunc: TUpDown
            Left = 198
            Top = 20
            Width = 16
            Height = 22
            Associate = BaseMinTrunc
            Enabled = False
            Max = 90
            TabOrder = 1
          end
          object BaseMinTrunc: TEdit
            Left = 175
            Top = 20
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '0'
          end
          object BtnFillBaseMinTrunc: TButton
            Left = 240
            Top = 19
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 3
            OnClick = BtnFillBaseMinTruncClick
          end
          object ForMinTr: TEdit
            Left = 215
            Top = 20
            Width = 25
            Height = 20
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            Text = '25'
          end
        end
        object BaseMaxSum: TGroupBox
          Left = 0
          Top = 51
          Width = 293
          Height = 57
          Align = alTop
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1084#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086#1081' '#1087#1083#1086#1097#1072#1076#1080' '#1090#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082#1072
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 1
          object Label3: TLabel
            Left = 122
            Top = 28
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object UseBaseMaxS: TCheckBox
            Left = 7
            Top = 27
            Width = 114
            Height = 17
            Caption = #1044#1077#1083#1072#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
            TabOrder = 0
            OnClick = UseBaseMaxSClick
          end
          object UpDownBaseMaxS: TUpDown
            Left = 198
            Top = 25
            Width = 16
            Height = 22
            Associate = BaseMaxS
            Enabled = False
            Max = 9999
            TabOrder = 1
          end
          object BaseMaxS: TEdit
            Left = 175
            Top = 25
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '0'
          end
          object BtnFillBaseMaxS: TButton
            Left = 239
            Top = 24
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 3
            OnClick = BtnFillBaseMaxSClick
          end
          object MaxS1: TEdit
            Left = 216
            Top = 25
            Width = 25
            Height = 20
            TabStop = False
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Ctl3D = True
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = False
            TabOrder = 4
            Text = '1'
          end
        end
        object BasePosition: TGroupBox
          Left = 0
          Top = 108
          Width = 293
          Height = 66
          Align = alTop
          Caption = #1055#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077
          TabOrder = 2
          object BasePosRough: TRadioButton
            Left = 8
            Top = 14
            Width = 153
            Height = 17
            Caption = #1043#1088#1091#1073#1086#1077' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077
            TabOrder = 0
          end
          object BasePosGravityCenter: TRadioButton
            Left = 8
            Top = 30
            Width = 145
            Height = 17
            Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091' '#1090#1103#1078#1077#1089#1090#1080
            TabOrder = 1
          end
          object BasePosDoNotUse: TRadioButton
            Left = 8
            Top = 46
            Width = 143
            Height = 17
            Caption = #1041#1077#1079' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1103
            Checked = True
            TabOrder = 2
            TabStop = True
          end
        end
      end
      object Addings: TTabSheet
        Caption = #1042#1090#1086#1088#1080#1095#1085#1099#1077' '#1086#1075#1088#1072#1085#1080#1095#1077#1085#1080#1103
        ImageIndex = 3
        object CheckMinTrInNewElements: TGroupBox
          Left = 0
          Top = 24
          Width = 313
          Height = 180
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1084#1080#1085'-'#1086#1075#1086' '#1091#1075#1083#1072' '#1085#1086#1074#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074' ('#1090#1088#1077#1091#1075'.)'
          TabOrder = 0
          object Label4: TLabel
            Left = 139
            Top = 20
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object BaseCheckTRInNewElements: TCheckBox
            Left = 8
            Top = 19
            Width = 123
            Height = 17
            Caption = #1044#1077#1083#1072#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
            TabOrder = 0
            OnClick = BaseCheckTRInNewElementsClick
          end
          object UpDownBaseMinTrInNewElement: TUpDown
            Left = 216
            Top = 17
            Width = 15
            Height = 22
            Associate = BaseMinTrInNewElement
            Enabled = False
            Max = 90
            TabOrder = 1
          end
          object BaseMinTrInNewElement: TEdit
            Left = 193
            Top = 17
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '0'
          end
          object BtnFillBaseMinTrInNewElement: TButton
            Left = 261
            Top = 17
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 3
            OnClick = BtnFillBaseMinTrInNewElementClick
          end
          object OutMinTrT: TEdit
            Left = 229
            Top = 18
            Width = 25
            Height = 20
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            Text = '23'
          end
        end
        object CheckMinTrInNewSElements: TGroupBox
          Left = 0
          Top = 80
          Width = 289
          Height = 73
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1085#1086#1074#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074' ('#1088#1072#1079#1073#1080#1077#1085#1080#1077' '#1089#1077#1075#1084#1077#1085#1090#1072')'
          TabOrder = 1
          object Label5: TLabel
            Left = 138
            Top = 20
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object Label6: TLabel
            Left = 139
            Top = 46
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object BaseCheckTRInNewSElements: TCheckBox
            Left = 8
            Top = 19
            Width = 123
            Height = 17
            Caption = #1052#1080#1085#1080#1084#1072#1083#1100#1085#1086#1075#1086' '#1091#1075#1083#1072
            TabOrder = 0
            OnClick = BaseCheckTRInNewSElementsClick
          end
          object UpDownBaseMinTrInNewSElement: TUpDown
            Left = 215
            Top = 17
            Width = 15
            Height = 22
            Associate = BaseMinTrInNewSElement
            Enabled = False
            Max = 90
            TabOrder = 1
          end
          object BaseMinTrInNewSElement: TEdit
            Left = 192
            Top = 17
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '0'
          end
          object BaseCheckMinSInNewSElements: TCheckBox
            Left = 9
            Top = 47
            Width = 122
            Height = 15
            Caption = #1052#1080#1085#1080#1084'-'#1086#1081' '#1087#1083#1086#1097#1072#1076#1080
            TabOrder = 3
            OnClick = BaseCheckMinSInNewSElementsClick
          end
          object UpDownBaseMinSInNewSElement: TUpDown
            Left = 216
            Top = 43
            Width = 15
            Height = 22
            Associate = BaseMinSInNewSElement
            Enabled = False
            Max = 9999
            TabOrder = 4
          end
          object BaseMinSInNewSElement: TEdit
            Left = 193
            Top = 43
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 5
            Text = '0'
          end
          object BtnFillBaseCheckTRInNewSElements: TButton
            Left = 263
            Top = 17
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 6
            OnClick = BtnFillBaseCheckTRInNewSElementsClick
          end
          object BtnFillBaseCheckMinSInNewSElements: TButton
            Left = 263
            Top = 43
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 7
            OnClick = BtnFillBaseCheckMinSInNewSElementsClick
          end
          object OutMinTrS: TEdit
            Left = 232
            Top = 18
            Width = 25
            Height = 20
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 8
            Text = '25'
          end
          object OutMinS: TEdit
            Left = 232
            Top = 44
            Width = 25
            Height = 20
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 9
            Text = '1'
          end
        end
      end
      object ChangeD: TTabSheet
        Caption = #1050#1088#1080#1090#1077#1088#1080#1080' '#1087#1088#1086#1094#1077#1076#1091#1088#1099' "'#1057#1084#1077#1085#1072' '#1076#1080#1072#1075#1086#1085#1072#1083#1077#1081'"'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        object Posiotion: TGroupBox
          Left = 0
          Top = 0
          Width = 293
          Height = 89
          Align = alTop
          Caption = #1055#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077
          TabOrder = 0
          object PosRough: TRadioButton
            Left = 8
            Top = 24
            Width = 153
            Height = 17
            Caption = #1043#1088#1091#1073#1086#1077' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077
            TabOrder = 0
          end
          object PosGravityCenter: TRadioButton
            Left = 8
            Top = 40
            Width = 145
            Height = 17
            Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091' '#1090#1103#1078#1077#1089#1090#1080
            TabOrder = 1
          end
          object PosDoNotUse: TRadioButton
            Left = 8
            Top = 56
            Width = 143
            Height = 17
            Caption = #1041#1077#1079' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1103
            Checked = True
            TabOrder = 2
            TabStop = True
          end
        end
        object CheckTR: TGroupBox
          Left = 0
          Top = 89
          Width = 293
          Height = 52
          Align = alTop
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1084#1080#1085#1080#1084#1072#1083#1100#1085#1086#1075#1086' '#1091#1075#1083#1072
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 1
          object Label1: TLabel
            Left = 123
            Top = 20
            Width = 51
            Height = 13
            Caption = #1047#1085#1072#1095#1077#1085#1080#1077':'
          end
          object UseMinTR: TCheckBox
            Left = 11
            Top = 19
            Width = 110
            Height = 17
            Caption = #1044#1077#1083#1072#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
            TabOrder = 0
            OnClick = UseMinTRClick
          end
          object UpDownMinTrunc: TUpDown
            Left = 200
            Top = 17
            Width = 16
            Height = 22
            Associate = MinTrunc
            Enabled = False
            Max = 90
            TabOrder = 1
          end
          object MinTrunc: TEdit
            Left = 177
            Top = 17
            Width = 23
            Height = 22
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Text = '0'
          end
          object BtnFillMinTrunc: TButton
            Left = 244
            Top = 17
            Width = 20
            Height = 22
            Caption = 'V'
            Enabled = False
            TabOrder = 3
            OnClick = BtnFillMinTruncClick
          end
          object MinTrForDiag: TEdit
            Left = 220
            Top = 18
            Width = 23
            Height = 20
            AutoSelect = False
            AutoSize = False
            BiDiMode = bdRightToLeft
            Color = clMoneyGreen
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = []
            MaxLength = 2
            ParentBiDiMode = False
            ParentFont = False
            ReadOnly = True
            TabOrder = 4
            Text = '25'
          end
        end
      end
      object MeshNRS: TTabSheet
        Caption = #1053#1077#1087#1088#1072#1074#1080#1083#1100#1085#1099#1081' '#1090#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082
        ImageIndex = 1
        TabVisible = False
        object NRS_TODO: TGroupBox
          Left = 0
          Top = 0
          Width = 293
          Height = 61
          Align = alTop
          Caption = 'E'#1089#1083#1080' '#1085#1072#1096#1083#1080' '#1091#1079#1077#1083', '#1090#1086
          TabOrder = 0
          object NRS_ChangeD: TRadioButton
            Left = 10
            Top = 20
            Width = 121
            Height = 17
            Caption = #1057#1084#1077#1085#1080#1084' '#1076#1080#1072#1075#1086#1085#1072#1083#1100
            TabOrder = 0
            OnClick = NRS_ChangeDClick
          end
          object NRS_AppNewNode: TRadioButton
            Left = 10
            Top = 40
            Width = 131
            Height = 17
            Caption = #1044#1086#1073#1072#1074#1080#1084' '#1085#1086#1074#1099#1081' '#1091#1079#1077#1083
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = NRS_AppNewNodeClick
          end
        end
        object NRS_Position: TGroupBox
          Left = 0
          Top = 139
          Width = 293
          Height = 73
          Align = alBottom
          Caption = #1057#1084#1077#1085#1072' '#1076#1080#1072#1075#1086#1085#1072#1083#1080
          Enabled = False
          TabOrder = 1
          object NRS_PosRough: TRadioButton
            Left = 8
            Top = 14
            Width = 153
            Height = 17
            Caption = #1043#1088#1091#1073#1086#1077' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077
            Enabled = False
            TabOrder = 0
          end
          object NRS_PosGravityCenter: TRadioButton
            Left = 8
            Top = 30
            Width = 145
            Height = 17
            Caption = #1055#1086' '#1094#1077#1085#1090#1088#1091' '#1090#1103#1078#1077#1089#1090#1080
            Enabled = False
            TabOrder = 1
          end
          object NRS_PosDoNotUse: TRadioButton
            Left = 8
            Top = 46
            Width = 143
            Height = 17
            Caption = #1041#1077#1079' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1103
            Checked = True
            Enabled = False
            TabOrder = 2
            TabStop = True
          end
        end
        object NRS_PosNewNode: TGroupBox
          Left = 0
          Top = 61
          Width = 293
          Height = 78
          Align = alClient
          Caption = #1055#1086#1083#1086#1078#1077#1085#1080#1077' '#1085#1086#1074#1086#1075#1086' '#1091#1079#1083#1072
          Enabled = False
          TabOrder = 2
          object NRS_Middle: TRadioButton
            Left = 10
            Top = 20
            Width = 131
            Height = 17
            Caption = #1055#1086#1089#1077#1088#1077#1076#1080#1085#1077
            Checked = True
            Enabled = False
            TabOrder = 0
            TabStop = True
          end
          object NRS_Optimum: TRadioButton
            Left = 10
            Top = 40
            Width = 113
            Height = 17
            Caption = #1054#1087#1090#1080#1084#1072#1083#1100#1085#1086#1077
            Enabled = False
            TabOrder = 1
          end
        end
      end
    end
  end
  object Operations: TGroupBox
    Left = 312
    Top = 0
    Width = 297
    Height = 281
    Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1072#1083#1075#1086#1088#1080#1090#1084#1099
    TabOrder = 2
    object ToDo: TCheckListBox
      Left = 2
      Top = 15
      Width = 293
      Height = 210
      Align = alTop
      ItemHeight = 13
      TabOrder = 0
    end
    object DeleleDo: TButton
      Left = 115
      Top = 233
      Width = 70
      Height = 32
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = DeleleDoClick
    end
    object UP: TBitBtn
      Left = 24
      Top = 232
      Width = 73
      Height = 33
      TabOrder = 3
      OnClick = UPClick
      Glyph.Data = {
        B61A0000424DB61A000000000000360000002800000046000000200000000100
        180000000000801A00000000000000000000000000000000000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000008000008000008000
        00800000800000800000800000800000800000800000800000FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000800000FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF0080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000800000FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000800000FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000800000800000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF0080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000800000800000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000800000
        800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00800000800000800000800000800000800000800000800000
        800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080
        0000800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080
        0000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00800000800000800000800000FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000}
      Layout = blGlyphBottom
    end
    object DOWN: TBitBtn
      Left = 197
      Top = 233
      Width = 70
      Height = 32
      TabOrder = 2
      OnClick = DOWNClick
      Glyph.Data = {
        B61A0000424DB61A000000000000360000002800000046000000200000000100
        180000000000801A00000000000000000000000000000000000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000
        00800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000800000FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        800000800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000800000
        800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00800000800000800000800000800000800000800000800000800000800000
        800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080000080
        0000800000800000800000800000800000800000800000800000FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF0080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000800000800000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF0080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000800000800000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000800000
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000800000800000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00800000800000800000800000800000800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00800000800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF0080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF008000008000008000008000008000008000008000
        00800000800000800000800000800000800000800000800000FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        8000008000008000008000008000008000008000008000008000008000008000
        00800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00800000800000800000800000
        8000008000008000008000008000008000008000008000008000008000008000
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00800000800000800000800000800000800000800000800000
        800000800000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0080000080
        0000800000800000800000800000800000800000800000800000800000800000
        800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF0080000080000080000080000080000080
        0000800000800000800000800000800000800000800000800000800000FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF0080000080000080000080000080000080000080000080000080000080
        0000800000800000800000800000800000FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF008000008000
        0080000080000080000080000080000080000080000080000080000080000080
        0000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000}
      Layout = blGlyphBottom
    end
  end
  object GroupBox1: TGroupBox
    Left = 2
    Top = 264
    Width = 305
    Height = 249
    Caption = #1042#1099#1073#1086#1088' '#1072#1083#1075#1086#1088#1080#1090#1084#1086#1074
    TabOrder = 3
    DesignSize = (
      305
      249)
    object Label7: TLabel
      Left = 8
      Top = 16
      Width = 114
      Height = 13
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077' '#1072#1083#1075#1086#1088#1080#1090#1084#1099':'
    end
    object Label8: TLabel
      Left = 8
      Top = 88
      Width = 150
      Height = 13
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1072#1083#1075#1086#1088#1080#1090#1084#1099':'
    end
    object SaveIfAlgWas: TButton
      Left = 24
      Top = 212
      Width = 252
      Height = 25
      Anchors = []
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Microsoft Sans Serif'
      Font.Pitch = fpFixed
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnClick = SaveIfAlgWasClick
    end
    object Calculate: TButton
      Left = 24
      Top = 184
      Width = 252
      Height = 25
      Anchors = []
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1072#1083#1075#1086#1088#1080#1090#1084#1099' '#1082' '#1089#1077#1090#1082#1077' '#1050#1069
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Microsoft Sans Serif'
      Font.Pitch = fpFixed
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = CalculateClick
    end
    object AddDo: TButton
      Left = 24
      Top = 157
      Width = 252
      Height = 25
      Anchors = []
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1083#1075#1086#1088#1080#1090#1084
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = AddDoClick
    end
    object ListOther: TListBox
      Left = 2
      Top = 103
      Width = 295
      Height = 42
      ItemHeight = 13
      Items.Strings = (
        #1055#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077' '#1091#1079#1083#1086#1074' ('#1043#1088#1091#1073#1086#1077' '#1087#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077')'
        #1055#1086#1079#1080#1094#1080#1086#1085#1080#1088#1086#1074#1072#1085#1080#1077' '#1091#1079#1083#1086#1074' ('#1055#1086' '#1094#1077#1085#1090#1088#1091' '#1090#1103#1078#1077#1089#1090#1080')')
      TabOrder = 3
      OnClick = ListOtherClick
    end
    object ListMain: TListBox
      Left = 0
      Top = 32
      Width = 298
      Height = 57
      ItemHeight = 13
      Items.Strings = (
        #1057#1084#1077#1085#1072' '#1076#1080#1072#1075#1086#1085#1072#1083#1077#1081
        #1056#1072#1079#1073#1080#1077#1085#1080#1077' '#1089#1077#1075#1084#1077#1085#1090#1072
        #1056#1072#1079#1073#1080#1077#1085#1080#1077' '#1082#1086#1085#1090#1091#1088#1072
        #1056#1072#1079#1073#1080#1077#1085#1080#1077' '#1090#1088#1077#1075#1086#1083#1100#1085#1080#1082#1072)
      TabOrder = 4
      OnClick = ListMainClick
    end
  end
end
